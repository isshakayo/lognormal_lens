/*
This code calculates the convergence auto power spectra with the source redshift distribution of pz(z).
See eq.(4.5) and (4.3) of Makiya et al. 2020, arXiv:2008.13195.
Note: The functions for double-expnential integration (intdeini and intede) are originally developed 
by Takuya OOURA (http://www.kurims.kyoto-u.ac.jp/~ooura/index.html).
 */

#include<iostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<cassert>
#include<cmath>
#include<vector>
#include<gsl/gsl_spline.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_integration.h>

using namespace std;

double PI = 4.*atan(1.0);
double c = 2.99792e5; 
string tmp_s;
double tmp_d;

int izs_min;
int izs_max;

int nbox;
int nk;
gsl_interp_accel *acc_chi2z = gsl_interp_accel_alloc();
gsl_interp_accel *acc_z2chi = gsl_interp_accel_alloc();
gsl_interp_accel *acc_pk = gsl_interp_accel_alloc();
gsl_interp_accel *acc_cl = gsl_interp_accel_alloc();
gsl_spline *pk_spl[200];
double z_in[200];
double dchi_in[200];
double zs_weight_arr[200];
double kmin_arr[200];
double kmax_arr[200];
int izs;


void read_input(string input);

double zs;
double chi_s;
double Om;
double Ol;
double h;

// comoving distance
double result, error;
gsl_integration_workspace *w = gsl_integration_workspace_alloc (10000);
gsl_function F;
int nz = 1000; 
gsl_spline *chi2z = gsl_spline_alloc(gsl_interp_cspline, nz);
gsl_spline *z2chi = gsl_spline_alloc(gsl_interp_cspline, nz);
double Ez(double z){
  double Ez = Om*pow((1+z),3)+Ol;
  Ez = sqrt(Ez);
  return Ez;
}
double dc_integrant(double x, void *params){
  return c/(h*100.)/Ez(x);
}
// return comoving distance in Mpc/h
double dc(double z){
    F.function = &dc_integrant;
    double alpha = 1.0;
    F.params = &alpha; // dummy
    gsl_integration_qags (&F, 0, z, 0, 1e-7, 1000,
                            w, &result, &error);
    return result*h;
}

double Wk(double chi);
double calc_clgk(double ell, int izg);
gsl_spline *cl_kk_spl;

int izg;
double bias;

void get_cl_discretized(gsl_spline *cl_spl, double *cl, double *ell, int *nMode);
int NGRID;
int MAX_K; // k=NGRID/2 is the nyquist for even NGRID
double BoxScale;  // 6.45 degree
double lunit;
double lmax;
double lmin;
double loglmin;
int nell_grid;
const float dlogl=0.3*log10(M_E); // follows Sato+2009  

int main(int argc, char **argv)
{
    if(argc!=11){
        cerr << "Usage: cl_model.exe ms_params.txt ofname NGRID FoV Om0 h izs_min izs_max izg bias\n\n";
        return -1;
    }

    string input = argv[1];
    string output = argv[2];
    NGRID = atoi(argv[3]);
    MAX_K = NGRID/2+1; // k=NGRID/2 is the nyquist for even NGRID
    BoxScale = atof(argv[4])*M_PI/180.0;  // 6.45 degree
    lunit = 2.0*M_PI/BoxScale;
    lmax = 2.0*M_PI*MAX_K/BoxScale;  // ~78000
    lmin = lunit;
    loglmin = log10(lmin);
    nell_grid = (int)((log10(lmax)-log10(lmin))/dlogl)+1;

    Om = atof(argv[5]);
    Ol = 1.0-Om;
    h = atof(argv[6]);
    izs_min = atoi(argv[7]);
    izs_max = atoi(argv[8]);
    izg = atoi(argv[9]); // index of galaxy slice
    bias = atof(argv[10]);
    double z_arr[nz];
    double chi_arr[nz];
    for(int i = 0; i<nz; i++){
        z_arr[i] = 1e-5+(5.0-1e-5)*i/(double)(nz);
        chi_arr[i] = dc(z_arr[i]);
    }
    gsl_spline_init(z2chi, z_arr, chi_arr, nz);
    gsl_spline_init(chi2z, chi_arr, z_arr, nz);

    // read mP(k) and chi_ms, del_chi, n_ms
    read_input(input);

    // normalize weights
    double norm = 0.0;
    for (int izs=izs_min; izs<izs_max; izs++) norm += zs_weight_arr[izs];
    for (int izs=izs_min; izs<izs_max; izs++) zs_weight_arr[izs] = zs_weight_arr[izs]/norm;
  
    // calculate Cl^kk
    int nell = 2000;
    double ell[nell];
    double cl_kk[nell];
    for (int i=0; i<nell; i++){
        ell[i] = pow(10,0+log10(20000.)/(double)nell*i);
        cl_kk[i] = calc_clgk(ell[i], izg);
    }
    cl_kk_spl = gsl_spline_alloc(gsl_interp_cspline, nell);
    gsl_spline_init(cl_kk_spl,ell,cl_kk,nell);

    // evaluate Cl on grid
    double ell_grid[nell_grid];
    double cl_kk_grid[nell_grid];
    int nMode[nell_grid];
    get_cl_discretized(cl_kk_spl, cl_kk_grid, ell_grid, nMode);

    // output
    ofstream fout(output.c_str());
    for(int i=0; i<nell_grid-1; i++){ // i=0: no skip for l=lmin
        fout << setiosflags(ios::scientific)
      	     << setprecision(5)
	         << ell_grid[i] << " "
             << cl_kk_grid[i] << " "
      	     << nMode[i] << endl;
    }
    fout.close();

    // free splines
    gsl_interp_accel_free(acc_pk);
    gsl_interp_accel_free(acc_cl);
    gsl_interp_accel_free(acc_chi2z);
    gsl_interp_accel_free(acc_z2chi);
    for (int i=0; i<200; i++){    
        gsl_spline_free(pk_spl[i]);
    }
    gsl_spline_free(cl_kk_spl);
    gsl_spline_free(chi2z);
    gsl_spline_free(z2chi);
}

void read_input(string input){
	ifstream rf(input.c_str()); assert(rf.is_open());
    while (getline(rf,tmp_s)){ // skip comments
        nbox++;
    }
    rf.clear();
    rf.seekg(0,ios_base::beg);
    string pkfname;
    for (int i=0; i<nbox; i++){
        rf >> pkfname >> z_in[i] >> dchi_in[i] >> zs_weight_arr[i];

        // read input mPk
   	    ifstream rf2(pkfname.c_str()); assert(rf2.is_open());
        nk = 0;
        while(getline(rf2,tmp_s)) nk++;
        rf2.clear();
        rf2.seekg(0,ios_base::beg);
        double k_in[nk]; double pk_in[nk];
        for (int j=0; j<nk; j++){
            rf2 >> k_in[j] >> pk_in[j];
        }
        kmin_arr[i] = k_in[0];
        kmax_arr[i] = k_in[nk-1];
        pk_spl[i] = gsl_spline_alloc(gsl_interp_cspline, nk);
        gsl_spline_init(pk_spl[i], k_in, pk_in, nk);
    }
}

double calc_Wk(double chi){ // single source redshift
    double z = gsl_spline_eval(chi2z, chi, acc_chi2z);
    double Wk = 0.0;
    for (int izs=izs_min; izs<izs_max; izs++){
        double chi_s = gsl_spline_eval(z2chi, z_in[izs], acc_z2chi);
        if (chi <= chi_s){
            Wk += zs_weight_arr[izs]*(1.0-chi/chi_s);
        }
    }
    Wk = 15000.0/c/c*Om*(1+z)*chi*Wk;
    return Wk;
}

double calc_clgk(double ell, int i){
    double chi_ms= gsl_spline_eval(z2chi, z_in[i], acc_z2chi);
    double k = (ell+0.5)/chi_ms;
    if (k < kmin_arr[i] or k > kmax_arr[i]){
        return 0.0;
    }
//    double chi_ms2= gsl_spline_eval(z2chi, z_in[i-1], acc_z2chi);
//    double k2 = (ell+0.5)/chi_ms2;
//cout << k << " " << k2 << endl;

    double mpk = gsl_spline_eval(pk_spl[i], k, acc_pk);
    double Wk = calc_Wk(chi_ms);
    double Wg = 1./dchi_in[i];
    double cl = Wk*Wg/pow(chi_ms,2)*bias*mpk*dchi_in[i];
    return cl;
}

void get_cl_discretized(gsl_spline *cl_spl, double *cl, double *ell, int *nMode)
{
    for (int i=0; i<nell_grid; i++){
        cl[i] = 0.0;
        ell[i] = 0.0;
        nMode[i] = 0;
    }

    double dl1;
    int il1, i1x, i1y;
    for(int i1x=-MAX_K+2; i1x<MAX_K; i1x++){
        for(int i1y=1; i1y<MAX_K; i1y++){
            dl1=lunit*sqrt((double)(i1x*i1x+i1y*i1y));
            if(dl1<lmax && dl1>=lmin){
      	        // il1=(int)((dl1-lmin)/lunit);
                il1=(int)((log10(dl1)-loglmin)/dlogl);
                cl[il1] += gsl_spline_eval(cl_spl, dl1, acc_cl)*dl1*dl1;
                ell[il1] += dl1;
	            nMode[il1]++;
            }
        }
    }
    for(int i1x=0; i1x<MAX_K; i1x++){
        dl1=lunit*((double)i1x);
        if(dl1<lmax && dl1>=lmin){
        // il1=(int)((dl1-lmin)/lunit);
        il1=(int)((log10(dl1)-loglmin)/dlogl);
        cl[il1] += gsl_spline_eval(cl_spl, dl1, acc_cl)*dl1*dl1;
        ell[il1] += dl1;
        nMode[il1]++;
        }
    }
    // for(int i=0; i<MAX_K; i++){
    for(int i=0; i<nell_grid; i++){
        if(nMode[i] > 0){
            cl[i] /= (double)nMode[i];
            ell[i] /= (double)nMode[i];
        }
    }
}

