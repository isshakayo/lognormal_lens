/*
This code calculates the convergence auto power spectra with the source redshift distribution of pz(z).
See eq.(4.2) and (4.3) of Makiya et al. 2020, arXiv:2008.13195.
Note: The functions for double-expnential integration (intdeini and intede) are originally developed 
by Takuya OOURA (http://www.kurims.kyoto-u.ac.jp/~ooura/index.html).
 */

#include<iostream>
#include<fstream>
#include<iomanip>
#include<string>
#include<cassert>
#include<cmath>
#include<vector>
#include<gsl/gsl_spline.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_integration.h>

using namespace std;

double PI = 4.*atan(1.0);
double c = 2.99792e5; 
string tmp_s;
double tmp_d;

int nbox;
int nk;
gsl_interp_accel *acc1 = gsl_interp_accel_alloc();
gsl_spline *pk_spl[200];
double z_in[200];
double dchi_in[200];
double zs_weight_arr[200];
double kmin_arr[200];
double kmax_arr[200];

void read_input(string input);
int izs_min;
int izs_max;
double zs;
double chi_s;
double Om;
double Ol;
double h;

// comoving distance
double result, error;
gsl_integration_workspace *w = gsl_integration_workspace_alloc (10000);
gsl_function F;
int nz = 1000; 
gsl_spline *chi2z = gsl_spline_alloc(gsl_interp_cspline, nz);
gsl_spline *z2chi = gsl_spline_alloc(gsl_interp_cspline, nz);
double Ez(double z){
  double Ez = Om*pow((1+z),3)+Ol;
  Ez = sqrt(Ez);
  return Ez;
}
double dc_integrant(double x, void *params){
  return c/(h*100.)/Ez(x);
}
// return comoving distance in Mpc/h
double dc(double z){
    F.function = &dc_integrant;
    double alpha = 1.0;
    F.params = &alpha; // dummy
    gsl_integration_qags (&F, 0, z, 0, 1e-7, 1000,
                            w, &result, &error);
    return result*h;
}

double Wk(double chi);
double calc_clkk(double ell);
gsl_spline *cl_kk_spl;

void get_cl_discretized(gsl_spline *cl_spl, double *cl, double *ell, int *nMode);
int NGRID;
int MAX_K; // k=NGRID/2 is the nyquist for even NGRID
double BoxScale;  // 6.45 degree
double lunit;
double lmax;
double lmin;
double loglmin;
int nell_grid;
const float dlogl=0.3*log10(M_E); // follows Sato+2009  

// for soource redshift weighting
double pz(double z){
    double z0 = 1./3.;
    return z*z*exp(-z/z0);
}

// functions for integration
void intdeini(int lenaw, double tiny, double eps, double *aw);
void intde(double (*f)(double), double a, double b, double *aw,double *i, double *err);


int main(int argc, char **argv)
{
    if(argc!=9){
        cerr << "Usage: cl_model.exe ms_params.txt ofname NGRID FoV Om0 h izmin izmax\n\n";
        return -1;
    }

    int lenaw=10000;
    double aw[lenaw];
    double err;
    intdeini(lenaw, 1.e-307, 1.e-8, aw);

    string input = argv[1];
    string output = argv[2];
    NGRID = atoi(argv[3]);
    MAX_K = NGRID/2+1; // k=NGRID/2 is the nyquist for even NGRID
    BoxScale = atof(argv[4])*M_PI/180.0;  // 6.45 degree
    lunit = 2.0*M_PI/BoxScale;
    lmax = 2.0*M_PI*MAX_K/BoxScale;  // ~78000
    lmin = lunit;
    loglmin = log10(lmin);
    nell_grid = (int)((log10(lmax)-log10(lmin))/dlogl)+1;

    Om = atof(argv[5]);
    Ol = 1.0-Om;
    h = atof(argv[6]);
    izs_min = atoi(argv[7]);
    izs_max = atoi(argv[8]);
    double z_arr[nz];
    double chi_arr[nz];
    for(int i = 0; i<nz; i++){
        z_arr[i] = 1e-5+(5.0-1e-5)*i/(double)(nz);
        chi_arr[i] = dc(z_arr[i]);
    }
    gsl_spline_init(z2chi, z_arr, chi_arr, nz);
    gsl_spline_init(chi2z, chi_arr, z_arr, nz);

    // read mP(k) and chi_ms, del_chi, n_ms
    read_input(input);

    // calculate Cl^kk
    int nell = 2000;
    double ell[nell];
    double cl_kk[nell];
    for (int i=0; i<nell; i++){
        ell[i] = pow(10,0+log10(20000.)/(double)nell*i);
        cl_kk[i] = 0.0;
    }

    // normalize weights
    double norm = 0.0;
    for (int izs=izs_min; izs<izs_max; izs++) norm += zs_weight_arr[izs];
    for (int izs=izs_min; izs<izs_max; izs++) zs_weight_arr[izs] = zs_weight_arr[izs]/norm;

    for (int i=0; i<nell; i++){
        cl_kk[i] += calc_clkk(ell[i]);
    }
    cl_kk_spl = gsl_spline_alloc(gsl_interp_cspline, nell);
    gsl_spline_init(cl_kk_spl,ell,cl_kk,nell);

    // evaluate Cl on grid
    double ell_grid[nell_grid];
    double cl_kk_grid[nell_grid];
    int nMode[nell_grid];
    get_cl_discretized(cl_kk_spl, cl_kk_grid, ell_grid, nMode);

    // output
    ofstream fout(output.c_str());
    for(int i=0; i<nell_grid-1; i++){ // i=0: no skip for l=lmin
        fout << setiosflags(ios::scientific)
      	     << setprecision(5)
	         << ell_grid[i] << " "
             << cl_kk_grid[i] << " "
      	     << nMode[i] << endl;
    }
    fout.close();

    // free splines
    gsl_interp_accel_free(acc1);
    for (int i=0; i<200; i++){    
        gsl_spline_free(pk_spl[i]);
    }
    gsl_spline_free(chi2z);
    gsl_spline_free(z2chi);
}

void read_input(string input){
	ifstream rf(input.c_str()); assert(rf.is_open());
    while (getline(rf,tmp_s)){ // skip comments
        nbox++;
    }
    rf.clear();
    rf.seekg(0,ios_base::beg);
    string pkfname;
    for (int i=0; i<nbox; i++){
        rf >> pkfname >> z_in[i] >> dchi_in[i] >> zs_weight_arr[i];

        // read input mPk
   	    ifstream rf2(pkfname.c_str()); assert(rf2.is_open());
        nk = 0;
        while(getline(rf2,tmp_s)) nk++;
        rf2.clear();
        rf2.seekg(0,ios_base::beg);
        double k_in[nk]; double pk_in[nk];
        for (int j=0; j<nk; j++){
            rf2 >> k_in[j] >> pk_in[j];
        }
        kmin_arr[i] = k_in[0];
        kmax_arr[i] = k_in[nk-1];
        pk_spl[i] = gsl_spline_alloc(gsl_interp_cspline, nk);
        gsl_spline_init(pk_spl[i], k_in, pk_in, nk);
    }
}

double calc_Wk(double chi){ // single source redshift
    double z = gsl_spline_eval(chi2z, chi, acc1);
    double Wk = 0.0;
    for (int izs=izs_min; izs<izs_max; izs++){
        double chi_s = gsl_spline_eval(z2chi, z_in[izs], acc1);
        if (chi <= chi_s){
            Wk += zs_weight_arr[izs]*(1.0-chi/chi_s);
        }
    }
    Wk = 15000.0/c/c*Om*(1+z)*chi*Wk;
    return Wk;
}

double calc_clkk(double ell){
    double cl = 0.0;
    for (int i=0; i<izs_max; i++){
        double chi_ms= gsl_spline_eval(z2chi, z_in[i], acc1);
        double k = (ell+0.5)/chi_ms;
        if (k < kmin_arr[i] or k > kmax_arr[i]){
            continue;
        }    
        double mpk = gsl_spline_eval(pk_spl[i], k, acc1);
        double Wk = calc_Wk(chi_ms);
        cl += pow(Wk,2)/pow(chi_ms,2)*mpk*dchi_in[i];
    }
    return cl;
}

void get_cl_discretized(gsl_spline *cl_spl, double *cl, double *ell, int *nMode)
{
    for (int i=0; i<nell_grid; i++){
        cl[i] = 0.0;
        ell[i] = 0.0;
        nMode[i] = 0;
    }

    double dl1;
    int il1, i1x, i1y;
    for(int i1x=-MAX_K+2; i1x<MAX_K; i1x++){
        for(int i1y=1; i1y<MAX_K; i1y++){
            dl1=lunit*sqrt((double)(i1x*i1x+i1y*i1y));
            if(dl1<lmax && dl1>=lmin){
      	        // il1=(int)((dl1-lmin)/lunit);
                il1=(int)((log10(dl1)-loglmin)/dlogl);
                // cl[il1] += gsl_spline_eval(cl_spl, dl1, acc1);
                cl[il1] += gsl_spline_eval(cl_spl, dl1, acc1)*dl1*dl1;
                ell[il1] += dl1;
	            nMode[il1]++;
            }
        }
    }
    for(int i1x=0; i1x<MAX_K; i1x++){
        dl1=lunit*((double)i1x);
        if(dl1<lmax && dl1>=lmin){
        // il1=(int)((dl1-lmin)/lunit);
        il1=(int)((log10(dl1)-loglmin)/dlogl);
        // cl[il1] += gsl_spline_eval(cl_spl, dl1, acc1);
        cl[il1] += gsl_spline_eval(cl_spl, dl1, acc1)*dl1*dl1;
        ell[il1] += dl1;
        nMode[il1]++;
        }
    }
    // for(int i=0; i<MAX_K; i++){
    for(int i=0; i<nell_grid; i++){
        if(nMode[i] > 0){
            cl[i] /= (double)nMode[i];
            ell[i] /= (double)nMode[i];
        }
    }
}

void intdeini(int lenaw, double tiny, double eps, double *aw)
{
    /* ---- adjustable parameter ---- */
    double efs = 0.1, hoff = 8.5;
    /* ------------------------------ */
    int noff, nk, k, j;
    double pi2, tinyln, epsln, h0, ehp, ehm, h, t, ep, em, xw, wg;
    
    pi2 = 2 * atan(1.0);
    tinyln = -log(tiny);
    epsln = 1 - log(efs * eps);
    h0 = hoff / epsln;
    ehp = exp(h0);
    ehm = 1 / ehp;
    aw[2] = eps;
    aw[3] = exp(-ehm * epsln);
    aw[4] = sqrt(efs * eps);
    noff = 5;
    aw[noff] = 0.5;
    aw[noff + 1] = h0;
    aw[noff + 2] = pi2 * h0 * 0.5;
    h = 2;
    nk = 0;
    k = noff + 3;
    do {
        t = h * 0.5;
        do {
            em = exp(h0 * t);
            ep = pi2 * em;
            em = pi2 / em;
            j = k;
            do {
                xw = 1 / (1 + exp(ep - em));
                wg = xw * (1 - xw) * h0;
                aw[j] = xw;
                aw[j + 1] = wg * 4;
                aw[j + 2] = wg * (ep + em);
                ep *= ehp;
                em *= ehm;
                j += 3;
            } while (ep < tinyln && j <= lenaw - 3);
            t += h;
            k += nk;
        } while (t < 1);
        h *= 0.5;
        if (nk == 0) {
            if (j > lenaw - 6) j -= 3;
            nk = j - noff;
            k += nk;
            aw[1] = nk;
        }
    } while (2 * k - noff - 3 <= lenaw);
    aw[0] = k - 3;
}



void intde(double (*f)(double), double a, double b, double *aw, 
    double *i, double *err)
{
    int noff, lenawm, nk, k, j, jtmp, jm, m, klim;
    double epsh, ba, ir, xa, fa, fb, errt, errh, errd, h, iback, irback;
    
    noff = 5;
    lenawm = (int) (aw[0] + 0.5);
    nk = (int) (aw[1] + 0.5);
    epsh = aw[4];
    ba = b - a;
    *i = (*f)((a + b) * aw[noff]);
    ir = *i * aw[noff + 1];
    *i *= aw[noff + 2];
    *err = fabs(*i);
    k = nk + noff;
    j = noff;
    do {
        j += 3;
        xa = ba * aw[j];
        fa = (*f)(a + xa);
        fb = (*f)(b - xa);
        ir += (fa + fb) * aw[j + 1];
        fa *= aw[j + 2];
        fb *= aw[j + 2];
        *i += fa + fb;
        *err += fabs(fa) + fabs(fb);
    } while (aw[j] > epsh && j < k);
    errt = *err * aw[3];
    errh = *err * epsh;
    errd = 1 + 2 * errh;
    jtmp = j;
    while (fabs(fa) > errt && j < k) {
        j += 3;
        fa = (*f)(a + ba * aw[j]);
        ir += fa * aw[j + 1];
        fa *= aw[j + 2];
        *i += fa;
    }
    jm = j;
    j = jtmp;
    while (fabs(fb) > errt && j < k) {
        j += 3;
        fb = (*f)(b - ba * aw[j]);
        ir += fb * aw[j + 1];
        fb *= aw[j + 2];
        *i += fb;
    }
    if (j < jm) jm = j;
    jm -= noff + 3;
    h = 1;
    m = 1;
    klim = k + nk;
    while (errd > errh && klim <= lenawm) {
        iback = *i;
        irback = ir;
        do {
            jtmp = k + jm;
            for (j = k + 3; j <= jtmp; j += 3) {
                xa = ba * aw[j];
                fa = (*f)(a + xa);
                fb = (*f)(b - xa);
                ir += (fa + fb) * aw[j + 1];
                *i += (fa + fb) * aw[j + 2];
            }
            k += nk;
            j = jtmp;
            do {
                j += 3;
                fa = (*f)(a + ba * aw[j]);
                ir += fa * aw[j + 1];
                fa *= aw[j + 2];
                *i += fa;
            } while (fabs(fa) > errt && j < k);
            j = jtmp;
            do {
                j += 3;
                fb = (*f)(b - ba * aw[j]);
                ir += fb * aw[j + 1];
                fb *= aw[j + 2];
                *i += fb;
            } while (fabs(fb) > errt && j < k);
        } while (k < klim);
        errd = h * (fabs(*i - 2 * iback) + fabs(ir - 2 * irback));
        h *= 0.5;
        m *= 2;
        klim = 2 * klim - noff;
    }
    *i *= h * ba;
    if (errd > errh) {
        *err = -errd * (m * fabs(ba));
    } else {
        *err = *err * aw[2] * (m * fabs(ba));
    }
}


