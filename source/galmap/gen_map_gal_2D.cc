#include<iostream>
#include<fstream>
#include<cmath>
#include<gsl/gsl_spline.h>
#include<gsl/gsl_integration.h>
#include<vector>
using namespace std;

namespace{
  int NGRIDDM;
  int NGRIDKP;
  
  double Lx, Ly, Lz;
  int Ngal;
  double dx, dy, dz;

  string ifname;
}

void readlognormal(string ifname, vector <vector<float> > &density);
double delta(double x, double y, vector< vector<float> > &density);

int main(int argc, char **argv){
  if(argc!=4){
    cerr << "usage: gen_map_gal_2D.exe ifname output NGRID \n\n";
    return -1;
  }

  ifname = argv[1];
  string ofname = argv[2];
  NGRIDDM = atoi(argv[3]);
  NGRIDKP = NGRIDDM;
  vector <vector<float> > density(NGRIDDM, vector<float>(NGRIDDM, 0));

  ////////
  readlognormal(ifname, density);
  float kappa[NGRIDKP][NGRIDKP];
  for(int i=0; i<NGRIDKP; i++){
    for(int j=0; j<NGRIDKP; j++){
      kappa[i][j]=0.0;
    }
  }

  for(int i=0; i<NGRIDKP; i++){
    for(int j=0; j<NGRIDKP; j++){
         float tempkappa = 0.0;
         kappa[i][j] += delta(dx*i,dx*j,density);
      }
    }

  //writeGalmap;
  fstream ofs;
  ofs.open(ofname.c_str(), ios::out);
  if (!ofs){
    cout << "cannot open " << ofname << endl;
    cout << "Exit" << endl;
    return 1;
  }
  for (int i=0; i<NGRIDKP; i++){
    for (int j=0; j<NGRIDKP; j++){
      ofs << density[i][j] << endl;
//        ofs << kappa[i][j] << endl;
    }
  }
}

void readlognormal(string ifname, vector< vector<float> > &density){
  fstream fs;
  fs.open(ifname.c_str(), ios::in | ios::binary);
  if (!fs){
    cout << "cannot open " << ifname << endl;
    cout << "Exit" << endl;
    return;
  }

  fs.read( (char*) &Lx, sizeof(double));
  fs.read( (char*) &Ly, sizeof(double));
  fs.read( (char*) &Lz, sizeof(double));
  fs.read( (char*) &Ngal, sizeof(int));
  cerr << "Lx: " << Lx << " Ly: " << Ly << " Lz: " << Lz << " Ngal: " << Ngal << endl;

  dx=Lx/(double)NGRIDDM;
  dy=Ly/(double)NGRIDDM;

  int ix, iy, iz;
  float pos_x, pos_y, pos_z, tmp;
  for (int i=0; i < Ngal; i++){
    fs.read( (char*) &pos_x, sizeof(float));
    fs.read( (char*) &pos_y, sizeof(float));
    fs.read( (char*) &pos_z, sizeof(float));
    fs.read( (char*) &tmp, sizeof(float));
    fs.read( (char*) &tmp, sizeof(float));
    fs.read( (char*) &tmp, sizeof(float));
    
    ix = int(pos_x/dx);
    iy = int(pos_y/dy);
    if(ix==NGRIDDM) ix=0;
    if(iy==NGRIDDM) iy=0;
    density[ix][iy] += 1.0;
  }
}

double delta(double x, double y, vector< vector<float> > &density){
  double ddx=x/dx;
  double ddy=y/dy;

  int ix=(int)ddx;
  int iy=(int)ddy;

  ddx-=ix;
  ddy-=iy;

  int ix1=ix+1;
  int iy1=iy+1;

  if(ix1==NGRIDKP) ix1=0;
  if(iy1==NGRIDKP) iy1=0;

  return (1-ddx)*(1-ddy)*density[ix][iy]
       + ddx    *(1-ddy)*density[ix1][iy]
       + (1-ddx)*ddy    *density[ix][iy1]
       + ddx    *ddy    *density[ix1][iy1];
}

