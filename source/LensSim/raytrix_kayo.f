!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      MODULE rt_psiarray
      implicit none
!!! data from GADGET2PSIS
c      real def1(ng,ng),def2(ng,ng) !def1=alpha_x, def2=alpha_y
      real, allocatable :: def1(:,:),def2(:,:)
c      real psi1(ng,ng),psi2(ng,ng),psi3(ng,ng) 
!           psi1=(psi_11+psi_22)=delta rho, psi2=(psi_11-psi_22), psi3=(psi_12)
      real, allocatable :: psi1(:,:),psi2(:,:),psi3(:,:)
      END MODULE rt_psiarray
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      program raytrix
      use rt_psiarray
  
c      implicit none
      implicit real (a-h,o-z)
      implicit integer (i-n)
      
      integer istatus,iunit,iblocksize,ibitpix,naxis,naxes(2)
      integer i,j,igroup,ifpixel,nelements
      integer pcount,gcount
      logical simple,extend,anyf
      character*88 comment,tmp

      integer ng, ngall, nplane, nsplane, nr, npmax

      common /omegas/ om,ov,ok
      common /wparam/ wDE
      common /curv/ sqok,iok
      common /defs/ pi,ch0,boxside,deltheta,r2d,r2am,r2as,field
      common /dim/ ng, ngall, nplane, nsplane, nr

      external dldz

!      parameter (ng=512) ! number of grids of input PSISs
!      parameter (ngall=ng*ng) ! do not touch
!      parameter (nplane=58) ! number of lens planes
!      parameter (nsplane=nplane-1) ! number of output source planes
!      parameter (nr=1024) ! number of grids for output

!!! scale factor, lensing kernels
!      real scalf(nplane)
!      real scalfzs(nsplane) !!!v3
!      real wdistm(nplane,nplane)
!      real wdistd(nplane,nplane)
!      real wdistms(nplane,nsplane) !!!v3
!      real wdistds(nplane,nsplane) !!!v3
      real, allocatable :: scalf(:)
      real, allocatable :: scalfzs(:)
      real, allocatable :: wdistm(:,:)
      real, allocatable :: wdistd(:,:)
      real, allocatable :: wdistms(:,:)
      real, allocatable :: wdistds(:,:)

!!! extention by kayo
!      real dint(nplane)  ! comoving interval of planes in Mpc/h
!      real dintch(nplane)
!      real coef(nplane)
!      real coefd(nplane)
      real, allocatable :: dint(:)  ! comoving interval of planes in Mpc/h
      real, allocatable :: dintch(:)
      real, allocatable :: coef(:)
      real, allocatable :: coefd(:)

!      character*7 pnum(nplane)
!      integer ishift_x(nplane)
!      integer ishift_y(nplane)
!      real boxsidelist(nplane)
!      real dsheet(nplane)
      character*7,allocatable :: pnum(:)
      integer,allocatable :: ishift_x(:)
      integer,allocatable :: ishift_y(:)
      real,allocatable :: boxsidelist(:)
      real,allocatable :: dsheet(:)

!      integer npout(nsplane)
      integer,allocatable :: npout(:)
      real ain, adis_ch, aout, comvd, comangd_j, comangd, pi
!      real aff(nplane)
      real,allocatable :: aff(:)
      integer isp, ipos, jpos, iplane
      real dgird, r2g, xshift, yshift, center, field

!!! angular positions of rays at the image plane
!      real tn(nr)
      real,allocatable :: tn(:)

      real, allocatable :: alp(:,:,:,:) !alp(2,nr,nr,nplane)
      real tmpalp(2)
c
      real, allocatable :: tau(:,:,:,:) !tau(2:4,nr,nr,nplane) 
!                          tau(2)=psi,11, tau(3)=psi,12=psi,21, tau(4)=psi,22 
      real tmptau(2:4)
!      real ampmat(2,2,nplane)
!      real zpout(nsplane) !!!v3
      real,allocatable :: ampmat(:,:,:)
      real,allocatable :: zpout(:) !!!v3

      real, allocatable :: postx(:,:),posty(:,:) !postx(nr,nr), posty(nr,nr)

      real, allocatable :: trace(:,:,:) !trace(nr,nr,nsplane)
      real, allocatable :: shea1(:,:,:),shea2(:,:,:) !shea1(nr,nr,nsplane)
                                                     !shea2(nr,nr,nsplane)
      real, allocatable :: rotat(:,:,:) !rotat(nr,nr,nsplane)
      real, allocatable :: outdata(:,:) !outdata(nr,nr)
c
      character*88 fname
      character*88 indir
c      character*7 pnum
      character*111 magfile
      character*111 alpfile
c
      character*20 runno
      character*88 outdir
      character*111 outname
      character*111 outlog
!      character*111 outdat(nsplane)
      character*111,allocatable :: outdat(:)


ccc read input file
      read(*,*)runno,ng,nplane,nr,hubblep,om,ov,wDE,deltheta
      write(*,*)runno,ng,nplane,nr,hubblep,om,ov,wDE,deltheta
      read(*,'(a)') indir
      read(*,'(a)') outdir
!      write(*,*) indir
!      write(*,*) outdir

      ngall=ng*ng
      nsplane=nplane-1

      outname=ADJUSTR(outdir)//'/'//ADJUSTL(runno)
      outlog=ADJUSTL(ADJUSTR(outname)//'.log')


ccc allocate arrays
      allocate(scalf(nplane))
      allocate(scalfzs(nsplane))
      allocate(wdistm(nplane,nplane))
      allocate(wdistd(nplane,nplane))
      allocate(wdistms(nplane,nsplane))
      allocate(wdistds(nplane,nsplane))
      allocate(dint(nplane))
      allocate(dintch(nplane))
      allocate(coef(nplane))
      allocate(coefd(nplane))
      allocate(pnum(nplane))
      allocate(ishift_x(nplane))
      allocate(ishift_y(nplane))
      allocate(boxsidelist(nplane))
      allocate(dsheet(nplane))
      allocate(npout(nsplane))
      allocate(aff(nplane))
      allocate(tn(nr))
      allocate(ampmat(2,2,nplane))
      allocate(zpout(nsplane))
      allocate(outdat(nsplane))

      allocate(def1(ng,ng))
      allocate(def2(ng,ng))
      allocate(psi1(ng,ng))
      allocate(psi2(ng,ng))
      allocate(psi3(ng,ng))
      allocate(alp(2,nr,nr,nplane))
      allocate(tau(2:4,nr,nr,nplane))
      allocate(trace(nr,nr,nsplane))
      allocate(shea1(nr,nr,nsplane))
      allocate(shea2(nr,nr,nsplane))
      allocate(rotat(nr,nr,nsplane))
      allocate(outdata(nr,nr))
      allocate(postx(nr,nr))
      allocate(posty(nr,nr))

c
      do iplane=1,nplane
         read(*,*)iplanein,pnum(iplane),ishift_x(iplane),
     &        ishift_y(iplane),boxsidelist(iplane),dsheet(iplane)
      enddo

c
ccc definitions
c
      pi=4.0*atan(1.0)
      ch0=2997.9
c      dint=120.0 ! comoving interval of planes in unit of Mpc/h
c      dintch=dint/ch0 ! in Mpc/h/2997.9

      dint(1)=dsheet(1)*0.5
      dintch(1)=dint(1)/ch0
      do iplane=2,nplane
         dint(iplane)=(dsheet(iplane-1)+dsheet(iplane))*0.5
         dintch(iplane)=dint(iplane)/ch0
      enddo

      r2d=180.0/pi ! radian -> degree
      r2am=180.0*60.0/pi ! radian -> arcmin
      r2as=180.0*60.0*60.0/pi ! radian -> arcsec
      field=deltheta*nr/60.0 ! in degree
c      coef=1.5*om*dintch
c      coefd=3.0*om*dintch
      do iplane=1,nplane
         coef(iplane)=1.5*om*dsheet(iplane)/ch0
         coefd(iplane)=3.0*om*dsheet(iplane)/ch0
      enddo
      center=0.5*ng

c
ccccc Computing redshift, distance,,, cccccccccccccccccccccc
c
 707  ok=om+ov-1.0
      sqok=sqrt(abs(ok))
      iok=0
      if (ok.lt.-1.0e-3) iok=-1
ccc computing the scale factor of i-th lens plane
      ain=0.99
      adis_ch=0.0
      do 40 iplane=1,nplane
         adis_ch=adis_ch+dintch(iplane)
         call newlap(adis_ch,aout,ain)
         scalf(iplane)=aout
         ain=aout
 40   continue
ccc source redshifts are placed on every mass sheet
      do i=1,nsplane
         scalfzs(i)=scalf(i+1)          ! mod by Kayo
         zpout(i)=1.0/scalfzs(i)-1.0  ! mod by Kayo
      enddo
ccc nearest planes for sources
c      do j=1,nsplane
c         npout(j)=nplane
c         do i=1,nplane
c            if (scalf(i).ge.scalfzs(j)) then
c               npout(j)=i-1
c               cycle
c            endif
c         enddo
c      enddo
      do i=1,nsplane ! mod by Kayo
         npout(i)=i+1
      enddo
      npmax=npout(nsplane)
ccc computing lensing kernel (lens-lens)
      aff(1)=dintch(1)
      do i=2,nplane
         aff(i)=aff(i-1)+dintch(i)
      enddo
      
      do 41 i=1,nplane-1
         do 42 j=i+1,nplane
            wdistm(i,j)=dfcomb(aff(i), aff(j))*coef(i)/scalf(i)
            wdistd(i,j)=angd(aff(j)-aff(i))*coefd(i)/scalf(i)
 42      continue
 41   continue
ccc computing lensing kernel (lens-source)
      do j=1,nsplane                             !!!v3
         call qromb(dldz,scalfzs(j),1.0,comvd)   !!!v3
c         affj=comvd ! distance to a source plane !!!v3
         do i=1,npout(j)                         !!!v3
c            affi=(i-0.5)*dintch ! distance to a lens plane 
            wdistms(i,j)=dfcomb(aff(i),comvd)*coef(i)/scalf(i)
            wdistds(i,j)=angd(comvd-aff(i))*coefd(i)/scalf(i)
         enddo
      enddo
ccc computing angular directions of rays on the image plane
      call angle(tn)

cccccccc log cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      open(88,file=outlog,STATUS='unknown')
 188  format(a50)
      write(88,188)'#=========== Cosmological parameters ============='
      write(88,884)'# Omega0  =',om
      write(88,884)'# Lambda0 =',ov
      write(88,884)'# h_100   =',hubblep
      write(88,884)'# w_DE    =',wDE
 884  format(a11,1x,1pe12.5)
      write(88,188)'#=========== Output pixel format ================='
      write(88,864)'# delta theta [arcmin] =',deltheta
      write(88,874)'# N_pixel =',nr,'x',nr
 864  format(a24,1x,1pe12.5)
 874  format(a11,1x,i5,a1,i4)
      write(88,188)'#========= redshifts of source planes ============'
      do 19 is=1,nsplane
         write(88,885)'# zs',is,'=',zpout(is) !!!v3
 19   continue
 885  format(a4,i2,a2,1pe12.5)
 881  format(a9,1x,i10)
      write(88,188)'#===== lens plane information ===================='
      write(88,882)'# col 1: lens plane number             '
      write(88,882)'# col 2: PSIS id                       '
      write(88,882)'# col 3: x-shift [boxsize=1]           '
      write(88,882)'# col 4: y-shift [boxsize=1]           '
      write(88,882)'# col 5: redshift (of middle of planes)'
 882  format(a39)
 883  format(i3,1x,a7,1x,1pe12.5,1x,1pe12.5,1x,1pe12.5)

c
ccc computing ray-path and psi,i and psi,ij at ray positions
c
ccc loop for planes
      do 77 iplane=1,npmax
c         read(*,*)iplanein,pnum,ishift_x,ishift_y,boxside
!!!!!!
         dgrid=boxsidelist(iplane)/ng
         r2g=ng/boxsidelist(iplane)
!!!!!!
ccc reading data and making random shifts
         xshift=real(ishift_x(iplane))
         yshift=real(ishift_y(iplane))
         uxs=1.0*xshift/(1.0*ng)
         uys=1.0*yshift/(1.0*ng)
         zlens=1.0/scalf(iplane)-1.0
         write(88,883)iplane,pnum(iplane),uxs,uys,zlens
c input data
         magfile=ADJUSTL(ADJUSTR(indir)//'/'//pnum(iplane))
         magfile=ADJUSTL(ADJUSTR(magfile)//'.mag')
         alpfile=ADJUSTL(ADJUSTR(indir)//'/'//pnum(iplane))
         alpfile=ADJUSTL(ADJUSTR(alpfile)//'.alp')
c
         open(10,file=magfile,form='unformatted',STATUS='old')
         read(10)psi1
         read(10)psi2
         read(10)psi3
         close(10)
         open(11,file=alpfile,form='unformatted',STATUS='old')
         read(11)def1
         read(11)def2
         close(11)
c
ccc determining the position of rays and computing tau
         if (iplane.eq.1) then
c            chi_j=0.5*dintch
            comangd_j=ch0*angd(aff(1))*r2g

            do 44 jpos=1,nr
               unposy=center+comangd_j*tn(jpos)
               forall (itmp=1:2) tmpalp(itmp)=0.0
               forall (itmp=2:4) tmptau(itmp)=0.0
               do 45 ipos=1,nr
                  unposx=center+comangd_j*tn(ipos)
                  call cic(unposx,unposy,ipos,jpos,iplane,
     $                 xshift,yshift,tmpalp,tmptau)
                  forall (itmp=1:2) 
     $                 alp(itmp,ipos,jpos,iplane)=tmpalp(itmp)
                  forall (itmp=2:4) 
     $                 tau(itmp,ipos,jpos,iplane)=tmptau(itmp)
 45            continue
 44         continue

         else
c            chi_j=dintch*(iplane-0.5)
            comangd=ch0*angd(aff(iplane))*r2g

            do 46 jpos=1,nr
               unposy=center+comangd*tn(jpos)
               do 47 ipos=1,nr
                  unposx=center+comangd*tn(ipos)
ccc calculation ray positions on j_th plane
                  sumx=0.0
                  sumy=0.0
                  do 51 jplane=1,iplane-1
                     sumx=sumx+alp(1,ipos,jpos,jplane)
     $                    *wdistd(jplane,iplane)
                     sumy=sumy+alp(2,ipos,jpos,jplane)
     $                    *wdistd(jplane,iplane)
 51               continue
                  posx=unposx-sumx*r2g
                  posy=unposy-sumy*r2g
ccc computing psi,i and psi,ij
                  forall (itmp=1:2) tmpalp(itmp)=0.0
                  forall (itmp=2:4) tmptau(itmp)=0.0
                  call cic(posx,posy,ipos,jpos,iplane,
     $                 xshift,yshift,tmpalp,tmptau)
                  forall (itmp=1:2) 
     $                 alp(itmp,ipos,jpos,iplane)=tmpalp(itmp)
                  forall (itmp=2:4) 
     $                 tau(itmp,ipos,jpos,iplane)=tmptau(itmp)
 47            continue
 46         continue
         endif
 77   continue
      write(88,188)'#================================================='
      close(88)

c
ccc computing ray-positions at source planes
c
      do isp=1,nsplane
         iplane=npout(isp)   ! source plane
         call qromb(dldz,scalfzs(isp),1.0,comvd) !!!v3
         comangd=ch0*angd(comvd)*r2g !!!v3
         do jpos=1,nr
            unposy=center+comangd*tn(jpos)
            do ipos=1,nr
               unposx=center+comangd*tn(ipos)
ccc calculation the position on j_th plane
               sumx=0.0
               sumy=0.0
               do jplane=1,iplane ! lens plane
                  sumx=sumx+alp(1,ipos,jpos,jplane)
     $                 *wdistds(jplane,isp)
                  sumy=sumy+alp(2,ipos,jpos,jplane)
     $                 *wdistds(jplane,isp)
               enddo
               posx=unposx-sumx*r2g
               posy=unposy-sumy*r2g
               postx(ipos,jpos)=((posx-center)/comangd)*r2d
               posty(ipos,jpos)=((posy-center)/comangd)*r2d
            enddo
         enddo
      enddo

cccccccccccc RAY-TRACING cccccccccccccccccccccccccccc     
      do 54 jpos=1,nr
         do 55 ipos=1,nr
c j=1
            ampmat(1,1,1)=1.0
            ampmat(1,2,1)=0.0
            ampmat(2,1,1)=0.0
            ampmat(2,2,1)=1.0
            ap11=tau(2,ipos,jpos,1)*ampmat(1,1,1)+
     $           tau(3,ipos,jpos,1)*ampmat(2,1,1)
            ap12=tau(2,ipos,jpos,1)*ampmat(1,2,1)+
     $           tau(3,ipos,jpos,1)*ampmat(2,2,1)
            ap21=tau(3,ipos,jpos,1)*ampmat(1,1,1)+
     $           tau(4,ipos,jpos,1)*ampmat(2,1,1)
            ap22=tau(3,ipos,jpos,1)*ampmat(1,2,1)+
     $           tau(4,ipos,jpos,1)*ampmat(2,2,1)
            ampmat(1,1,1)=ap11
            ampmat(1,2,1)=ap12
            ampmat(2,1,1)=ap21
            ampmat(2,2,1)=ap22
c
            isp=1
            do 60 j=2,npmax
               ampmat(1,1,j)=1.0
               ampmat(1,2,j)=0.0
               ampmat(2,1,j)=0.0
               ampmat(2,2,j)=1.0
               do 61 i=1,j-1
                  ampmat(1,1,j)=ampmat(1,1,j)+
     $                 ampmat(1,1,i)*wdistm(i,j)
                  ampmat(1,2,j)=ampmat(1,2,j)+
     $                 ampmat(1,2,i)*wdistm(i,j)
                  ampmat(2,1,j)=ampmat(2,1,j)+
     $                 ampmat(2,1,i)*wdistm(i,j)
                  ampmat(2,2,j)=ampmat(2,2,j)+
     $                 ampmat(2,2,i)*wdistm(i,j)
 61            continue
               ap11=tau(2,ipos,jpos,j)*ampmat(1,1,j)+
     $              tau(3,ipos,jpos,j)*ampmat(2,1,j)
               ap12=tau(2,ipos,jpos,j)*ampmat(1,2,j)+
     $              tau(3,ipos,jpos,j)*ampmat(2,2,j)
               ap21=tau(3,ipos,jpos,j)*ampmat(1,1,j)+
     $              tau(4,ipos,jpos,j)*ampmat(2,1,j)
               ap22=tau(3,ipos,jpos,j)*ampmat(1,2,j)+
     $              tau(4,ipos,jpos,j)*ampmat(2,2,j)
               ampmat(1,1,j)=ap11
               ampmat(1,2,j)=ap12
               ampmat(2,1,j)=ap21
               ampmat(2,2,j)=ap22
c
               if (j.eq.npout(isp)) then
                  ap11=1.0
                  ap12=0.0
                  ap21=0.0
                  ap22=1.0
                  do i=1,j
                     ap11=ap11+ampmat(1,1,i)*wdistms(i,isp)
                     ap12=ap12+ampmat(1,2,i)*wdistms(i,isp)
                     ap21=ap21+ampmat(2,1,i)*wdistms(i,isp)
                     ap22=ap22+ampmat(2,2,i)*wdistms(i,isp)
                  enddo
                  trace(ipos,jpos,isp)=1.0-0.5*(ap11+ap22)
                  shea1(ipos,jpos,isp)=-0.5*(ap11-ap22)
                  shea2(ipos,jpos,isp)=-0.5*(ap12+ap21)
                  rotat(ipos,jpos,isp)=-0.5*(ap12-ap21)
                  isp=isp+1
               endif
 60         continue
 55      continue
 54   continue

!
!!!!! data output
!
      do is=1,nsplane
         write(tmp,*) is
         outdat(is)=ADJUSTL(ADJUSTR(outname)//'.'
     $        //trim(ADJUSTL(tmp))//'.dat')
         open(100,file=outdat(is),
     $        form='unformatted',STATUS='unknown')
         write(100)nr,deltheta,zpout(is)           ! mod by Kayo
         write(100)((trace(i,j,is),i=1,nr),j=1,nr)
         write(100)((shea1(i,j,is),i=1,nr),j=1,nr)
         write(100)((shea2(i,j,is),i=1,nr),j=1,nr)
         write(100)((rotat(i,j,is),i=1,nr),j=1,nr)
         close(100)
      enddo

ccc  deallocate arrays
      deallocate(def1)
      deallocate(def2)
      deallocate(psi1)
      deallocate(psi2)
      deallocate(psi3)
      deallocate(alp)
      deallocate(tau)
      deallocate(trace)
      deallocate(shea1)
      deallocate(shea2)
      deallocate(rotat)
      deallocate(outdata)
      deallocate(postx)
      deallocate(posty)
      end program raytrix

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccc  package of calculating the distance combination
ccc  Dd*Dds/Ds
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      function dfcomb(xl,xs)
      implicit real (a-h,o-z)
      implicit integer (i-n)
      common /omegas/ om,ov,ok
      common /curv/ sqok,iok
      if (iok.eq.0) then
         dfl=xl
         dfs=xs
         dfls=xs-xl
      else
         dfl=sinh(sqok*xl)/sqok
         dfs=sinh(sqok*xs)/sqok
         dfls=sinh(sqok*(xs-xl))/sqok
      endif
      dfcomb=dfl*dfls/dfs
      return
      end

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccc  CIC interpolation for tau (linear interpolation)
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine cic(px,py,i,j,np,xshift,yshift,alp,tau)
      use rt_psiarray
      implicit real (a-h,o-z)
      implicit integer (i-n)
      common /defs/ pi,ch0,boxside,deltheta,r2d,r2am,r2as,field
      common /dim/ ng, ngall, nplane, nsplane, nr
!      parameter (ng=512) ! number of grids of input PSISs
!      parameter (ngtemp=512) ! number of grids of input PSISs
!      parameter (ngall=ng*ng) ! do not touch
!      parameter (nplane=58) ! number of lens planes
!      parameter (nsplane=nplane-1) ! number of output source planes
!      parameter (nr=1024) ! number of grids for output
!      include 'raytrix_kayo.par'
      real alp(2)
      real tau(2:4)
      dimension wx(2)
      dimension wy(2)
      x=px+xshift
      y=py+yshift
      ix=nint(x)
      iy=nint(y)
      delx=x-real(ix)
      dely=y-real(iy)
      if (delx.ge.0.0) then
         jx=ix+1
         wx(1)=1.0-delx
         wx(2)=delx
      else
         jx=ix-1
         wx(1)=1.0+delx
         wx(2)=-delx
      endif
      if (dely.ge.0.0) then
         jy=iy+1
         wy(1)=1.0-dely
         wy(2)=dely
      else
         jy=iy-1
         wy(1)=1.0+dely
         wy(2)=-dely
      endif
      call periodic(ix,ng)
      call periodic(iy,ng)
      call periodic(jx,ng)
      call periodic(jy,ng)
      wii=wx(1)*wy(1)
      wij=wx(1)*wy(2)
      wji=wx(2)*wy(1)
      wjj=wx(2)*wy(2)
      ricci=wii*psi1(ix,iy)+wij*psi1(ix,jy)
     $     +wji*psi1(jx,iy)+wjj*psi1(jx,jy)
      weyl1=wii*psi2(ix,iy)+wij*psi2(ix,jy)
     $     +wji*psi2(jx,iy)+wjj*psi2(jx,jy)
      weyl2=wii*psi3(ix,iy)+wij*psi3(ix,jy)
     $     +wji*psi3(jx,iy)+wjj*psi3(jx,jy)
      tau(2)=-ricci-weyl1
      tau(3)=-2.0*weyl2
      tau(4)=-ricci+weyl1
      alp(1)=wii*def1(ix,iy)+wij*def1(ix,jy)
     $     +wji*def1(jx,iy)+wjj*def1(jx,jy)
      alp(2)=wii*def2(ix,iy)+wij*def2(ix,jy)
     $     +wji*def2(jx,iy)+wjj*def2(jx,jy)
      return
      end
ccccc
      subroutine periodic(ip,ng)
      implicit none
      integer ip, ng
 110  if (ip.lt.1) then
         ip=ng+ip
         goto 110
      endif
 111  if (ip.gt.ng) then
         ip=ip-ng
         goto 111
      endif
      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccc  package of calculating the angular diameter distance
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      function angd(x)
      implicit real (a-h,o-z)
      implicit integer (i-n)
      common /omegas/ om,ov,ok
      common /curv/ sqok,iok
      if (iok.eq.0) then
         angd=x
      else
         angd=sinh(sqok*x)/sqok
      endif
      return
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc      
ccc  Newton-Raphson method
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine newlap(alm,aout,ain)
      implicit real (a-h,o-z)
      implicit integer (i-n)
      parameter (eps=1.0e-5)
      external dldz
      rhs=alm
      a=ain
 356  call qromb(dldz,a,1.0,solv)
      err=abs(solv/rhs-1.0)
      if (err.gt.eps) then
         dela=-(rhs-solv)/dldz(a)
         a=a+dela
         goto 356
      endif
      aout=a
      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc      
ccc  package of solving the integral equation of 
ccc  affine parameter - redshit (scale factor) relation
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      function dldz(a)
      implicit real (a-h,o-z)
      implicit integer (i-n)
      common /omegas/ om,ov,ok
      common /wparam/ wDE
      a2=a*a
      dldz=1.0/sqrt(a*om-a2*ok+ov*a**(1.0-3.0*wDE))
      return
      end

ccc
      SUBROUTINE qromb(func,a,b,ss)
      INTEGER JMAX,JMAXP,K,KM
      REAL a,b,func,ss,EPS
      EXTERNAL func
      PARAMETER (EPS=1.e-6, JMAX=20, JMAXP=JMAX+1, K=5, KM=K-1)
CU    USES polint,trapzd
      INTEGER j
      REAL dss,h(JMAXP),s(JMAXP)
      h(1)=1.
      do 11 j=1,JMAX
        call trapzd(func,a,b,s(j),j)
        if (j.ge.K) then
          call polint(h(j-KM),s(j-KM),K,0.,ss,dss)
          if (abs(dss).le.EPS*abs(ss)) return
        endif
        s(j+1)=s(j)
        h(j+1)=0.25*h(j)
11    continue
      write(*,*)'too many steps in qromb'
      stop
      END
ccc
      SUBROUTINE trapzd(func,a,b,s,n)
      INTEGER n
      REAL a,b,s,func
      EXTERNAL func
      INTEGER it,j
      REAL del,sum,tnm,x
      if (n.eq.1) then
        s=0.5*(b-a)*(func(a)+func(b))
      else
        it=2**(n-2)
        tnm=it
        del=(b-a)/tnm
        x=a+0.5*del
        sum=0.
        do 11 j=1,it
          sum=sum+func(x)
          x=x+del
11      continue
        s=0.5*(s+(b-a)*sum/tnm)
      endif
      return
      END
ccc
      SUBROUTINE polint(xa,ya,n,x,y,dy)
      INTEGER n,NMAX
      REAL dy,x,y,xa(n),ya(n)
      PARAMETER (NMAX=10)
      INTEGER i,m,ns
      REAL den,dif,dift,ho,hp,w,c(NMAX),d(NMAX)
      ns=1
      dif=abs(x-xa(1))
      do 11 i=1,n
        dift=abs(x-xa(i))
        if (dift.lt.dif) then
          ns=i
          dif=dift
        endif
        c(i)=ya(i)
        d(i)=ya(i)
11    continue
      y=ya(ns)
      ns=ns-1
      do 13 m=1,n-1
        do 12 i=1,n-m
          ho=xa(i)-x
          hp=xa(i+m)-x
          w=c(i+1)-d(i)
          den=ho-hp
          if (den.eq.0.) then
             write(*,*)'failure in polint'
             stop
          endif
          den=w/den
          d(i)=hp*den
          c(i)=ho*den
12      continue
        if (2*ns.lt.n-m)then
          dy=c(ns+1)
        else
          dy=d(ns)
          ns=ns-1
        endif
        y=y+dy
13    continue
      return
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine angle(tn)
      implicit real (a-h,o-z)
      implicit integer (i-n)
      common /defs/ pi,ch0,boxside,deltheta,r2d,r2am,r2as,field
      common /dim/ ng, ngall, nplane, nsplane, nr
!      parameter (ng=512) ! number of grids of input PSISs
!      parameter (ngall=ng*ng) ! do not touch
!      parameter (nplane=58) ! number of lens planes
!      parameter (nsplane=nplane-1) ! number of output source planes
!      parameter (nr=1024) ! number of grids for output
!      include 'raytrix_kayo.par'
      real tn(nr)
      nr2=nr/2
      tn_op = tan(deltheta*nr/r2am/2.)*2
      do 10 i=1,nr
         theta=(i-nr2)*deltheta/r2am
         angular=tan(theta)
!         tn(i)=angular
         tn(i)=(-0.5+float(i)/float(nr))*tn_op
 10   continue
      return
      end

