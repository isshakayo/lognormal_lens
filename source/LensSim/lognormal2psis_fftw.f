ccc lognormal2psis_fftw.f
c  ifort compile option: -mcmodel=large -i-dynamic
c                        -L/usr/local/lib -lfftw3f
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      MODULE sdens_array
      implicit none
      integer ng
      integer nslice
      real boxsize
      real,allocatable :: df(:,:,:)
      END MODULE sdens_array
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      program lognormal2psis_fftw
      use sdens_array

      implicit none

      integer narg,i,j,im,ii,ir,jm,jj,kb,kx,ky,kx2,i0,i1,ierr
      real pi,vnpall,uwave,uflex,rr,op1,op2,op111,op112,op122,op222

      integer ng2, ngh, ngall

      real,allocatable :: fft(:,:)
      real,allocatable :: fft1(:,:), fft2(:,:)
      real,allocatable :: fft111(:,:),fft112(:,:)
      real,allocatable :: fft122(:,:),fft222(:,:)
      real,allocatable :: out1(:,:),out2(:,:),out3(:,:),out4(:,:)
      real,allocatable :: conv(:,:)

      character*111 fname
      integer*8 plan

      character*88 opt,arg

      include 'fftw3.f'
      
      pi=4.0*atan(1.0)

! set up parameters
      narg=iargc()
      do i=1,narg
         call getarg(i,opt)
         call getarg(i+1,arg)
         select case (opt)
         case ('-s')
            read(arg,*)nslice  ! number of slices
         case ('-n')
            read(arg,*)ng
         end select
      enddo

      ng2=2*ng
      ngh=ng/2
      ngall=ng*ng

      write(*,*) nslice,ng,ng2,ngh,ngall

ccc FFT
      vnpall=1.0/(1.0*ngall)

ccc allocate and clear array
      allocate(df(ng,ng,nslice))
      forall (kx=1:ng,ky=1:ng,kb=1:nslice) df(kx,ky,kb)=0.0

      allocate(fft(ng2,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(fft1(ng2,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(fft2(ng2,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(fft111(ng2,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(fft112(ng2,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(fft122(ng2,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(fft222(ng2,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(out1(ng,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(out2(ng,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(out3(ng,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(out4(ng,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'
      allocate(conv(ng,ng),stat=ierr)
      if(ierr.ne.0) write(*,*) 'failed\n'

ccc set boxsize
      uwave=boxsize/(2.0*pi)
      uflex=2.0*pi/boxsize

ccc reading Lognormal grid density data and generate the surface density
      call readlognormalfile()
!
! FFTing the surface mass density map
!
      do kb=1,nslice ! loop for lens planes
         do ky=1,ng
            do kx=1,ng
               kx2=2*kx
               conv(kx,ky)=df(kx,ky,kb)
               fft(kx2-1,ky)=conv(kx,ky)*vnpall
               fft(kx2,ky)=0.0
            enddo
         enddo
! Sigma -> hat(Sigma)
         call sfftw_plan_dft_2d(plan,ng,ng,fft,fft,
     &        FFTW_FORWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft,fft)
         call sfftw_destroy_plan(plan)
!
! compute Psi_11-Psi_22, Psi_12
! 
         forall (i=1:ng2,j=1:ng)
            fft1(i,j)=0.0
            fft2(i,j)=0.0
         end forall
         do j=1,ng
            if (j.le.ngh) then
               jm=j-1
            else
               jm=j-ng-1
            endif
            jj=jm*jm
            do i=1,ng
               if (i.eq.1.and.j.eq.1) cycle
               i1=2*i
               i0=i1-1
               if (i.le.ngh) then
                  im=i-1
               else
                  im=i-ng-1
               endif
               ii=im*im
               ir=jj+ii
               rr=1.0/(1.0*ir)
               op1=rr*(ii-jj)
               op2=rr*im*jm       
               fft1(i0,j)=fft(i0,j)*op1
               fft1(i1,j)=fft(i1,j)*op1
               fft2(i0,j)=fft(i0,j)*op2
               fft2(i1,j)=fft(i1,j)*op2
            enddo
         enddo
! hat(Psi_11-Psi_22) -> Psi_11-Psi_22
         call sfftw_plan_dft_2d(plan,ng,ng,fft1,fft1,
     &     FFTW_BACKWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft1,fft1)
         call sfftw_destroy_plan(plan)
! hat(Psi_12) -> Psi_12
         call sfftw_plan_dft_2d(plan,ng,ng,fft2,fft2,
     &        FFTW_BACKWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft2,fft2)
         call sfftw_destroy_plan(plan)
         do ky=1,ng
            do kx=1,ng
               kx2=2*kx-1
               out1(kx,ky)=fft1(kx2,ky) ! Psi_11-Psi_22
               out2(kx,ky)=fft2(kx2,ky) ! Psi_12
            enddo
         enddo
         read(*,'(a111)')fname
         open(20,file=fname,form='unformatted',STATUS='unknown')
         write(20)conv
         write(20)out1
         write(20)out2
         close(20)
!
! compute Psi_x and Psi_y
!          
         forall (i=1:ng2,j=1:ng)
            fft1(i,j)=0.0
            fft2(i,j)=0.0
         end forall
         do j=1,ng
            if (j.le.ngh) then
               jm=j-1
            else
               jm=j-ng-1
            endif
            jj=jm*jm
            do i=1,ng
               if (i.eq.1.and.j.eq.1) cycle
               i1=2*i
               i0=i1-1
               if (i.le.ngh) then
                  im=i-1
               else
                  im=i-ng-1
               endif
               ii=im*im
               ir=jj+ii
               rr=uwave/(1.0*ir)
               op1=rr*im
               op2=rr*jm
               fft1(i0,j)=op1*fft(i1,j)
               fft1(i1,j)=-op1*fft(i0,j)
               fft2(i0,j)=op2*fft(i1,j)
               fft2(i1,j)=-op2*fft(i0,j)
            enddo
         enddo
! hat(Psi_1) -> Psi_1
         call sfftw_plan_dft_2d(plan,ng,ng,fft1,fft1,
     &     FFTW_BACKWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft1,fft1)
         call sfftw_destroy_plan(plan)
! hat(Psi_2) -> Psi_2
         call sfftw_plan_dft_2d(plan,ng,ng,fft2,fft2,
     &        FFTW_BACKWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft2,fft2)
         call sfftw_destroy_plan(plan)
         do ky=1,ng
            do kx=1,ng
               kx2=2*kx-1
               out1(kx,ky)=fft1(kx2,ky) ! Psi_1
               out2(kx,ky)=fft2(kx2,ky) ! Psi_2
            enddo
         enddo
         read(*,'(a111)')fname
         open(20,file=fname,form='unformatted',STATUS='unknown')
         write(20)out1
         write(20)out2
         close(20)
!
! compute Psi_111, Psi_112, Psi_122 anf Psi_222
!          
         forall (i=1:ng2,j=1:ng)
            fft111(i,j)=0.0
            fft112(i,j)=0.0
            fft122(i,j)=0.0
            fft222(i,j)=0.0
         end forall
         do j=1,ng
            if (j.le.ngh) then
               jm=j-1
            else
               jm=j-ng-1
            endif
            jj=jm*jm
            do i=1,ng
               if (i.eq.1.and.j.eq.1) cycle
               i1=2*i
               i0=i1-1
               if (i.le.ngh) then
                  im=i-1
               else
                  im=i-ng-1
               endif
               ii=im*im
               ir=jj+ii
               rr=uflex/(1.0*ir)
               op111=rr*im*ii
               op112=rr*ii*jm
               op122=rr*im*jj
               op222=rr*jm*jj
               fft111(i0,j)=-op111*fft(i1,j)
               fft111(i1,j)=op111*fft(i0,j)
               fft112(i0,j)=-op112*fft(i1,j)
               fft112(i1,j)=op112*fft(i0,j)
               fft122(i0,j)=-op122*fft(i1,j)
               fft122(i1,j)=op122*fft(i0,j)
               fft222(i0,j)=-op222*fft(i1,j)
               fft222(i1,j)=op222*fft(i0,j)
            enddo
         enddo
! hat(Psi_111) -> Psi_111
         call sfftw_plan_dft_2d(plan,ng,ng,fft111,fft111,
     &     FFTW_BACKWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft111,fft111)
         call sfftw_destroy_plan(plan)
! hat(Psi_112) -> Psi_112
         call sfftw_plan_dft_2d(plan,ng,ng,fft112,fft112,
     &        FFTW_BACKWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft112,fft112)
         call sfftw_destroy_plan(plan)
! hat(Psi_122) -> Psi_122
         call sfftw_plan_dft_2d(plan,ng,ng,fft122,fft122,
     &     FFTW_BACKWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft122,fft122)
         call sfftw_destroy_plan(plan)
! hat(Psi_222) -> Psi_222
         call sfftw_plan_dft_2d(plan,ng,ng,fft222,fft222,
     &        FFTW_BACKWARD,FFTW_ESTIMATE+FFTW_UNALIGNED)
         call sfftw_execute_dft(plan,fft222,fft222)
         call sfftw_destroy_plan(plan)
         do ky=1,ng
            do kx=1,ng
               kx2=2*kx-1
               out1(kx,ky)=fft111(kx2,ky) ! Psi_111
               out2(kx,ky)=fft112(kx2,ky) ! Psi_112
               out3(kx,ky)=fft122(kx2,ky) ! Psi_111
               out4(kx,ky)=fft222(kx2,ky) ! Psi_112
            enddo
         enddo
         read(*,'(a111)')fname
         open(20,file=fname,form='unformatted',STATUS='unknown')
         write(20)out1
         write(20)out2
         write(20)out3
         write(20)out4
         close(20)
      enddo
      end program lognormal2psis_fftw

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine readlognormalfile()
      use sdens_array
      implicit none

      integer i,j,k,inslice

      character*200 inputfile
      
      real(8) :: Lx, Ly, Lz, tempdel
      integer(4) :: n0, n1, n2
      integer ngrid(nslice)

      read(*,'(a200)') inputfile
      inputfile=inputfile(1:len_trim(inputfile))

! start reading..
      open (1, file=inputfile, form='unformatted', access='stream')
      read (1) Lx,Ly,Lz,n0,n1,n2
      write (*,*) Lx,Ly,Lz,n0,n1,n2

      boxsize = Lx

      if(n0.ne.ng.or.n1.ne.ng) then
         close(1)
         write(*,*) "n0(n1) and ng is inconsistent"
         stop
      endif

      inslice = n2/nslice+1

      forall (i=1:nslice) ngrid(i)=0
      do k=1,n2
         ngrid(k/inslice+1)=ngrid(k/inslice+1)+1
      enddo

!      do i=1,nslice
!         write(*,*) ngrid(i)
!      enddo
      
      do i=1,n0
         do j=1,n1
            do k=1,n2
               read (1) tempdel
               df(i,j,k/inslice+1)=df(i,j,k/inslice+1)
     &              +tempdel/ngrid(k/inslice+1)
            end do
         end do
      end do

      close(1)

      return
      end
