#include<iostream>
#include<fstream>
#include<string>
#include<cmath>
using namespace std;

#include"gsl/gsl_math.h"
#include"gsl/gsl_rng.h"
#include"gsl/gsl_randist.h"

namespace{
  //const double sigma_init=0.374;
  //const double R_shear=1.7; // shear responsivity
  const double ellp=0.22;
  double ng; // in arcmin^-2
  
//  const int NGRID=512;
//  const float BoxScale=7.0; // in degree
  int NGRID;
  float BoxScale;
  float deltheta;
  // const double dB=BoxScale/(double)NGRID;
  // double Area_pix = dB*dB*3600.0; // in arcmin^2
  double dB;
  double Area_pix; // in arcmin^2

}

#define SKIP fin.read((char*)&dummy, sizeof(int))

void readKappa(string filename, float **kappa);


int main(int argc, char **argv){
  if(argc !=7){
    cerr << "usage: addnoise ng input output seed NGRID BoxScale\n\n";
    return -1;
  }

  double ng=atof(argv[1]);
  string input=argv[2];
  string output=argv[3];
  int seed = atoi(argv[4]);
  NGRID = atoi(argv[5]);
  BoxScale = atof(argv[6]);
  dB=BoxScale/(double)NGRID;
  Area_pix = dB*dB*3600.0; // in arcmin^2

  // reading kappa map
  float **kappa=new float*[NGRID];
  kappa[0]=new float[NGRID*NGRID];
  for(int i=1; i<NGRID; i++) kappa[i]=kappa[i-1]+NGRID;
  
  readKappa(input, kappa);

  // generating gaussian noise
  double sigmaN=ellp/sqrt(ng*Area_pix);
  
  gsl_rng *r = gsl_rng_alloc(gsl_rng_default);
  // gsl_rng_set(r, time(NULL));
  gsl_rng_set(r, seed);

  for(int i=0; i<NGRID; i++){
    for(int j=0; j<NGRID; j++){
      kappa[i][j]+=(float)gsl_ran_gaussian(r, sigmaN);
    }
  }

  // output

  ofstream fout(output.c_str(),ios::binary);
  int dummy=0;
  fout.write((char*)&dummy, sizeof(int));
  fout.write((char*)&NGRID, sizeof(int));
  fout.write((char*)&deltheta, sizeof(float));
  float dummyz=0;
  fout.write((char*)&dummyz, sizeof(float));
  fout.write((char*)&dummy, sizeof(int));
  fout.write((char*)&dummy, sizeof(int));
  fout.write((char*)kappa[0], sizeof(float)*NGRID*NGRID);
  fout.write((char*)&dummy, sizeof(int));

  fout.close();
}

void readKappa(string filename, float **kappa){
  ifstream fin(filename.c_str(), ios::binary|ios::in);
  int dummy;
  float dummyf, zpout;
  
  SKIP;
  fin.read((char*)&dummy, sizeof(int));
  cerr << "nr: " << dummy << " ";
  if(dummy!=NGRID){
    cerr<< "NGRID and nr are inconsistent!\n\n";
  }
    
  fin.read((char*)&deltheta, sizeof(float));
  cerr << "deltheta: " << deltheta << " ";
  fin.read((char*)&zpout, sizeof(float));
  cerr << "zpout: " << zpout << endl;
  SKIP;
  SKIP;
  fin.read((char*)kappa[0], sizeof(float)*NGRID*NGRID);

  fin.close();
}
