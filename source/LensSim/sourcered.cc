#include<iostream>
#include<cmath>
#include<fstream>
#include<string>
#include<sstream>
#include<cstdlib>
using namespace std;

#define SKIP fin.read((char*)&dummy, sizeof(int))

namespace{
  int NGRID;
  double z0=1.0/3.0;
  float deltheta;
  int nFile = 0;
}

void readKappa(string filename, float **kappa, float &zpout);
void intdeini(int lenaw, double tiny, double eps, double *aw);
void intde(double (*f)(double), double a, double b, double *aw, 
	   double *i, double *err);

double pz(double z){
  return z*z*exp(-z/z0);
}
 
int main(int argc, char **argv){
  if(argc != 6){
    cerr << "usage: sourcered basename iFile_min zs_weights.txt NGRID output\n\n";
    return -1;
  }
  string basename=argv[1];
  int iFile_min=atoi(argv[2])-1;
  string zs_weights_fname = argv[3];
  NGRID=atoi(argv[4]);
  string output=argv[5];

  // read zs_weights
  ifstream rf(zs_weights_fname.c_str());
  if(!rf){
    cerr << ", oops, the file not found!\n\n";
    exit(1);
  }
  string tmp_s;
  getline(rf,tmp_s); // skip izs=0
  while (getline(rf,tmp_s)){ // skip comments
    nFile++;
  }
  rf.clear();
  rf.seekg(0,ios_base::beg);
  double zs_weights[nFile];
  getline(rf,tmp_s); // skip izs=0
  for (int i=0; i<nFile; i++){
    rf >> zs_weights[i];
  }

  float ***kappa=new float**[nFile];
  float *tmp=new float[nFile*NGRID*NGRID];
  for(int i=0; i<nFile; i++){
    kappa[i]=new float*[NGRID];
    for(int j=0; j<NGRID; j++){
      kappa[i][j]=tmp+(i*NGRID+j)*NGRID;
    }
  }

  float *zpout=new float[nFile];

  for(int i=0; i<nFile; i++){
    string input;
    stringstream sbuf;
    sbuf << i+1;
    input=basename+"."+sbuf.str()+".dat";
    cout << "reading " << input << "...";
    readKappa(input, kappa[i], zpout[i]);
    // cout << "i = " << i << ", zpout = " << zpout[i] << endl;
  }


  float **kappaM=new float*[NGRID];
  kappaM[0]=new float[NGRID*NGRID];
  for(int i=1; i<NGRID; i++) kappaM[i]=kappaM[i-1]+NGRID;

  for(int i=0; i<NGRID; i++)
    for(int j=0; j<NGRID; j++)
      kappaM[i][j]=0.0;
  
  double norm = 0.0;
  for (int iFile=iFile_min; iFile<nFile; iFile++) norm += zs_weights[iFile];
  for (int iFile=iFile_min; iFile<nFile; iFile++){  
    double weight = zs_weights[iFile]/norm;
    for(int i=0; i<NGRID; i++){
      for(int j=0; j<NGRID; j++){
        kappaM[i][j] += weight*kappa[iFile][i][j];
      }
    }
  } 

  ofstream fout(output.c_str(),ios::binary);
  int dummy=0;
  fout.write((char*)&dummy, sizeof(int));
  fout.write((char*)&NGRID, sizeof(int));
  fout.write((char*)&deltheta, sizeof(float));
  float dummyz=0;
  fout.write((char*)&dummyz, sizeof(float));
  fout.write((char*)&dummy, sizeof(int));
  fout.write((char*)&dummy, sizeof(int));
  fout.write((char*)kappaM[0], sizeof(float)*NGRID*NGRID);
  fout.write((char*)&dummy, sizeof(int));

}

void readKappa(string filename, float **kappa, float &zpout){
  ifstream fin(filename.c_str(), ios::binary|ios::in);
  if(!fin){
    cerr << ", oops, the file not found!\n\n";
    exit(1);
  }

  int dummy;
  float dummyf;
  
  SKIP;
  fin.read((char*)&dummy, sizeof(int));
  cerr << "nr: " << dummy << " ";
  if(dummy!=NGRID){
    cerr<< "NGRID and nr are inconsistent!\n\n";
  }
    
  fin.read((char*)&deltheta, sizeof(float));
  cerr << "deltheta: " << deltheta << " ";
  fin.read((char*)&zpout, sizeof(float));
  cerr << "zpout: " << zpout << endl;
  SKIP;
  SKIP;
  fin.read((char*)kappa[0], sizeof(float)*NGRID*NGRID);

  fin.close();
}

void intdeini(int lenaw, double tiny, double eps, double *aw)
{
    /* ---- adjustable parameter ---- */
    double efs = 0.1, hoff = 8.5;
    /* ------------------------------ */
    int noff, nk, k, j;
    double pi2, tinyln, epsln, h0, ehp, ehm, h, t, ep, em, xw, wg;
    
    pi2 = 2 * atan(1.0);
    tinyln = -log(tiny);
    epsln = 1 - log(efs * eps);
    h0 = hoff / epsln;
    ehp = exp(h0);
    ehm = 1 / ehp;
    aw[2] = eps;
    aw[3] = exp(-ehm * epsln);
    aw[4] = sqrt(efs * eps);
    noff = 5;
    aw[noff] = 0.5;
    aw[noff + 1] = h0;
    aw[noff + 2] = pi2 * h0 * 0.5;
    h = 2;
    nk = 0;
    k = noff + 3;
    do {
        t = h * 0.5;
        do {
            em = exp(h0 * t);
            ep = pi2 * em;
            em = pi2 / em;
            j = k;
            do {
                xw = 1 / (1 + exp(ep - em));
                wg = xw * (1 - xw) * h0;
                aw[j] = xw;
                aw[j + 1] = wg * 4;
                aw[j + 2] = wg * (ep + em);
                ep *= ehp;
                em *= ehm;
                j += 3;
            } while (ep < tinyln && j <= lenaw - 3);
            t += h;
            k += nk;
        } while (t < 1);
        h *= 0.5;
        if (nk == 0) {
            if (j > lenaw - 6) j -= 3;
            nk = j - noff;
            k += nk;
            aw[1] = nk;
        }
    } while (2 * k - noff - 3 <= lenaw);
    aw[0] = k - 3;
}


void intde(double (*f)(double), double a, double b, double *aw, 
    double *i, double *err)
{
    int noff, lenawm, nk, k, j, jtmp, jm, m, klim;
    double epsh, ba, ir, xa, fa, fb, errt, errh, errd, h, iback, irback;
    
    noff = 5;
    lenawm = (int) (aw[0] + 0.5);
    nk = (int) (aw[1] + 0.5);
    epsh = aw[4];
    ba = b - a;
    *i = (*f)((a + b) * aw[noff]);
    ir = *i * aw[noff + 1];
    *i *= aw[noff + 2];
    *err = fabs(*i);
    k = nk + noff;
    j = noff;
    do {
        j += 3;
        xa = ba * aw[j];
        fa = (*f)(a + xa);
        fb = (*f)(b - xa);
        ir += (fa + fb) * aw[j + 1];
        fa *= aw[j + 2];
        fb *= aw[j + 2];
        *i += fa + fb;
        *err += fabs(fa) + fabs(fb);
    } while (aw[j] > epsh && j < k);
    errt = *err * aw[3];
    errh = *err * epsh;
    errd = 1 + 2 * errh;
    jtmp = j;
    while (fabs(fa) > errt && j < k) {
        j += 3;
        fa = (*f)(a + ba * aw[j]);
        ir += fa * aw[j + 1];
        fa *= aw[j + 2];
        *i += fa;
    }
    jm = j;
    j = jtmp;
    while (fabs(fb) > errt && j < k) {
        j += 3;
        fb = (*f)(b - ba * aw[j]);
        ir += fb * aw[j + 1];
        fb *= aw[j + 2];
        *i += fb;
    }
    if (j < jm) jm = j;
    jm -= noff + 3;
    h = 1;
    m = 1;
    klim = k + nk;
    while (errd > errh && klim <= lenawm) {
        iback = *i;
        irback = ir;
        do {
            jtmp = k + jm;
            for (j = k + 3; j <= jtmp; j += 3) {
                xa = ba * aw[j];
                fa = (*f)(a + xa);
                fb = (*f)(b - xa);
                ir += (fa + fb) * aw[j + 1];
                *i += (fa + fb) * aw[j + 2];
            }
            k += nk;
            j = jtmp;
            do {
                j += 3;
                fa = (*f)(a + ba * aw[j]);
                ir += fa * aw[j + 1];
                fa *= aw[j + 2];
                *i += fa;
            } while (fabs(fa) > errt && j < k);
            j = jtmp;
            do {
                j += 3;
                fb = (*f)(b - ba * aw[j]);
                ir += fb * aw[j + 1];
                fb *= aw[j + 2];
                *i += fb;
            } while (fabs(fb) > errt && j < k);
        } while (k < klim);
        errd = h * (fabs(*i - 2 * iback) + fabs(ir - 2 * irback));
        h *= 0.5;
        m *= 2;
        klim = 2 * klim - noff;
    }
    *i *= h * ba;
    if (errd > errh) {
        *err = -errd * (m * fabs(ba));
    } else {
        *err = *err * aw[2] * (m * fabs(ba));
    }
}
