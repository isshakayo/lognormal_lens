#include<iostream>
#include<fstream>
#include<cmath>
#include<string>
#include<complex>
#include<iomanip>
#include<stdlib.h>
using namespace std;

#include"fftw3.h"

namespace{
  int NGRID; // should be even
  int MAX_K; // k=NGRID/2 is the nyquist for even NGRID
  int NGRIDMAXK;
  int NGRIDSURVEY;
  float BoxScale;  // 6.45 degree
  double lunit;
  float lmin;// = 72;
  float loglmin; //=log10(lmin);
  float lmax;  // ~78000
  float lmax2;
  float lunit2;
  const float dlogl=0.3*log10(M_E); // follows Sato+2009  
  int NL;
}

#define SKIP fin.read((char*)&dummy, sizeof(int)) // read dummy


complex<float> wrapkappa(complex<float> **kappa_k, int ix, int iy){
  if(iy>=0){
    if(ix>=0)
      return kappa_k[ix][iy];
    else
      return kappa_k[NGRID+ix][iy];
  }
  else{
    return conj(wrapkappa(kappa_k, -ix, -iy));
  }
}

/* proto types */
void readKappa(string filename, float **kappa);
void readGal(string filename, float **gal);

void fft_RFFTW(float **grid_density, complex<float> **delta_k);

void get_cross(complex<float> **kappa_k, complex<float> **gal_k, 
	       double *pl_k, double *l, int *nMode);


int main(int argc, char **argv)
{
  if(argc!=6){
    cerr << "Usage: get_cross kappa gal output NGRID OpeningAngle\n\n";
    return -1;
  }

  string input1=argv[1];
  string input2=argv[2];
  string output=argv[3];
  NGRID=atoi(argv[4]);
  MAX_K=NGRID/2+1; // k=NGRID/2 is the nyquist for even NGRID
  NGRIDMAXK=NGRID*MAX_K;
  NGRIDSURVEY=NGRIDMAXK-(MAX_K-2);
  BoxScale=atof(argv[5]) * M_PI/180.0;  // 6.45 degree
  lunit=2.0*M_PI/BoxScale;
  lmax=2.0*M_PI*MAX_K/BoxScale;  // ~78000
  lmin = lunit;
  loglmin = log10(lmin);
  lmax2=lmax*lmax;
  lunit2=lunit*lunit;
  NL=(int)((log10(lmax)-log10(lmin))/dlogl)+1;
 
  float **kappa=new float*[NGRID];
  kappa[0]=new float[NGRID*NGRID];
  for(int i=1; i<NGRID; i++) kappa[i]=kappa[i-1]+NGRID;

  readKappa(input1, kappa);

  complex<float> **kappa_k=new complex<float>*[NGRID];
  kappa_k[0]=new complex<float>[NGRIDMAXK];
  for(int i=1; i<NGRID; i++) kappa_k[i]=kappa_k[i-1]+MAX_K;
  fft_RFFTW(kappa, kappa_k);

  delete[] kappa[0];
  delete[] kappa;


  float **gal=new float*[NGRID];
  gal[0]=new float[NGRID*NGRID];
  for(int i=1; i<NGRID; i++) gal[i]=gal[i-1]+NGRID;
  
  readGal(input2, gal);

  complex<float> **gal_k=new complex<float>*[NGRID];
  gal_k[0]=new complex<float>[NGRIDMAXK];
  for(int i=1; i<NGRID; i++) gal_k[i]=gal_k[i-1]+MAX_K;
  fft_RFFTW(gal, gal_k);

  delete[] gal[0];
  delete[] gal;

  double pl_k[NL], l[NL];
  int nMode[NL];
  get_cross(kappa_k, gal_k, pl_k, l, nMode);
  
  delete[] kappa_k[0];
  delete[] kappa_k;
  delete[] gal_k[0];
  delete[] gal_k;

  ofstream fout(output.c_str());
  float BoxArea=BoxScale*BoxScale;

  for(int i=0;i<NL-1;i++){ // i=0: no skip for l=lmin
    fout << setiosflags(ios::scientific)
      	 << setprecision(5)
	       << l[i] << " "
         << pl_k[i]/BoxArea << " "
	       << nMode[i] << endl;
  }
  fout.close();
}

void readKappa(string filename, float **kappa){
  ifstream fin(filename.c_str(), ios::binary|ios::in);
  int dummy;
  float dummyf;
  
  SKIP;
  fin.read((char*)&dummy, sizeof(int));
  cerr << "nr: " << dummy << endl;
  fin.read((char*)&dummyf, sizeof(float));
  cerr << "deltheta: " << dummyf << endl;
  fin.read((char*)&dummyf, sizeof(float));
  cerr << "zpout: " << dummyf << endl;
  SKIP;
  SKIP;
  fin.read((char*)kappa[0], sizeof(float)*NGRID*NGRID);
}

void readGal(string filename, float **gal){
  ifstream fin(filename.c_str());

  double tempx, tempy;
  double sum=0.0;
  
  for(int i=0; i<NGRID; i++){
    for(int j=0; j<NGRID; j++){
      fin >> gal[j][i];
      sum+=gal[j][i];
    }
  }

  cerr << "ngal:" << sum << endl;
  
  for(int i=0; i<NGRID; i++)
    for(int j=0; j<NGRID; j++)
      gal[i][j] = gal[i][j]/(sum/(double)(NGRID*NGRID)) -1.0;
  // density fluctuations
  
}

void fft_RFFTW(float **kappa, complex<float> **kappa_k)
{
  fftwf_plan plan;
  
  plan = fftwf_plan_dft_r2c_2d(NGRID, NGRID, kappa[0], (fftwf_complex*)kappa_k[0], FFTW_ESTIMATE);

  fftwf_execute(plan);

  fftwf_destroy_plan(plan);

  float scales=BoxScale*BoxScale/(float)(NGRID*NGRID);
  for(int i=0; i < NGRID; i++){
    for(int j=0; j < NGRID/2+1; j++){
      kappa_k[i][j] *= scales;
    }
  }
}


void get_cross(complex<float> **kappa_k, complex<float> **gal_k, double *pl_k, double *l, int *nMode)
{

  for(int i=0;i<NL;i++){
    pl_k[i]=0.0;
    l[i]=0.0;
    nMode[i]=0;
  }

  double dl1;
  int il1, i1x, i1y;

  for(int i1x=-MAX_K+2; i1x<MAX_K; i1x++){
    for(int i1y=1; i1y<MAX_K; i1y++){
      dl1=lunit*sqrt((double)(i1x*i1x+i1y*i1y));
    
      if(dl1<lmax && dl1>=lmin){
        il1=(int)((log10(dl1)-loglmin)/dlogl);
        pl_k[il1] += real(wrapkappa(kappa_k, i1x, i1y)*conj(wrapkappa(gal_k, i1x, i1y)))
                    *dl1*dl1;
        l[il1]+=dl1;
        nMode[il1]++;
      }
    }
  }

  for(int i1x=0; i1x<MAX_K; i1x++){
    dl1=lunit*((double)i1x);
    if(dl1<lmax && dl1>=lmin){
      il1=(int)((log10(dl1)-loglmin)/dlogl);
      pl_k[il1] += real(wrapkappa(kappa_k, i1x, 0)*conj(wrapkappa(gal_k, i1x, 0)))
                  *dl1*dl1;
      l[il1]+=dl1;
      nMode[il1]++;
    }
  }

  for(int i=0;i<NL;i++){
    if(nMode[i] > 0){
      pl_k[i] /= (double)nMode[i];
      l[i] /= (double)nMode[i];
    }
  }
}

