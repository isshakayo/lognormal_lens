# Data descriptions
- Cross spectra
    - the files under ./cl_gk_sim are the simulated HSC x PFS cross-spectra at each redshift. They are the average of 30 realizations of 7 x 7 deg^2 fields simulation.
- Covariance
    - the files under ./cov are the covarice matricies at each redshift. They are estimated from 200 realizations of cross spectra and galaxy power spectra in redshift space (i.e., 200 x 30 realizations of 7 x 7 deg^2 field simulation).
- Model cross spectra
    - the files under ./cl_gk_model are the HSC x PFS cross spectra calculated from the input power spectra. See eq.(4.1), (4.3) and (4.5) for the details of the calculation 
