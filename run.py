#!/usr/bin/env python
#============================================================================

import sys
import os
import subprocess
import random
import numpy as np
import astropy.units
from astropy.cosmology import FlatLambdaCDM, z_at_value
from docopt import docopt
from classy import Class
import configparser
import json
import time
from scipy.integrate import quad
from joblib import Parallel, delayed

__doc__ = """{f}

Usage:
    {f} <inifile>
    {f} -c | --check <inifile>
    {f} -h | --help

Options:
    -c, --check           show raytracing geometry for visual checking
    -h, --help            show this help and exit.

Example:
    {f} <inifile>       run with <inifile>. 
                      
""".format(f=__file__)

def make_dir(dir_name):
    try:
        os.makedirs(dir_name)
        print('Directory '+dir_name+' has been created')
    except:
        print('Directory '+dir_name+' exists already - continue to use this')

def pz(z):
    z0 = 1./3.
    return z*z*np.exp(-z/z0)

##### read .ini file ###########
ini = configparser.ConfigParser()
args = docopt(__doc__)
ini.read(args['<inifile>'])

check=False
if args['--check']:
    check = True
    prefix = "dummy"
    seed = 0
else:
    prefix = ini['settings']['prefix']
    seed = ini['settings'].getint('seed')

######### Setting parameters #################
# directories (relative to this 'run.py' file)
out_dir = ini['settings']['out_dir']
out_dir = os.path.join(out_dir,prefix)
make_dir(out_dir)
raytrix_dir = os.path.join(os.getcwd(),'./source/LensSim')
lognormal_galaxies_dir = ini['settings']['lognormal_galaxies_dir']
if not os.path.exists(lognormal_galaxies_dir): 
    print(lognormal_galaxies_dir+' does not exist!\nquit.')
    sys.exit()

# cosmological parameters 
ob0h2, oc0h2, lnAs, ns, run, h0, mnu, w = [ini['params'].getfloat(p) for p in 
                                        ['ob0h2','oc0h2','lnAs','ns','run','h0','mnu','w']]

# resolution and FoV
PnmaxFid = ini['params'].getint('Nmesh')
NgridRay = PnmaxFid

# raytracing configuration
OpeningAngle = ini['params'].getfloat('OpeningAngle')  # in degree

NboxGalaxy = ini['params'].getint('NboxGalaxy')
NboxFore = ini['params'].getint('NboxFore')
NboxBack = ini['params'].getint('NboxBack')
NboxAll = NboxFore+NboxGalaxy+NboxBack
zEndBack = ini['params'].getfloat('zEndBack')
if ini['params'].get('nMassSheets'):
    nMassSheets = json.loads(ini['params'].get('nMassSheets'))
else:
    nMassSheets = [1 for x in range(NboxAll)]

zStart_arr = json.loads(ini['params'].get('zStart_arr'))
zEnd_arr = json.loads(ini['params'].get('zEnd_arr'))
ng_arr = json.loads(ini['params'].get('ng_arr'))
bias_arr = json.loads(ini['params'].get('bias_arr'))

num_para = ini['settings'].getint('num_para')

# for input power spectrum
cosmo_class = Class()
As = np.exp(lnAs)*1e-10
pars = {'output':'mPk',
        'h':h0,'omega_b':ob0h2,'omega_cdm':oc0h2,\
        'A_s':As,'n_s':ns,'alpha_s':run,\
        'Omega_Lambda':0.0,'w0_fld':w,\
        'N_ur':0.00641,'N_ncdm':1,'m_ncdm':mnu/3.,\
        'T_ncdm':0.71611,\
        'P_k_max_h/Mpc': 3.5e2,'z_max_pk':3.2,\
        'deg_ncdm':3.,'non linear':None}
cosmo_class.set(pars)
print("Computing input P(k)...")
cosmo_class.compute()
kh_arr = np.logspace(-4.,2.5,1000)
pk_zarr = (np.array(zStart_arr)+np.array(zEnd_arr))/2.
mpk_dir = out_dir+'/inputs'
make_dir(mpk_dir)
mpk_fnames = []

# settings for ini files of lognormal_galaxies 
inifiles_dir =  lognormal_galaxies_dir+'/'+prefix+'_inifiles'
make_dir(inifiles_dir)
lg_inifiles_name = [inifiles_dir+'/'+prefix+'_'+str(i)+'.ini' for i in range(NboxAll)]

# settings for lensdata outputs
lensdata_dir = out_dir+'/lensdata'
make_dir(lensdata_dir)

# settings for 2D galmap
galmap_dir = out_dir+'/galmap'
try:
    os.makedirs(galmap_dir)
except:
    pass
make_galmap_dir = os.path.join(os.getcwd(),'./source/galmap')

# settingss for Cl calculation
power_dir = os.path.join(os.getcwd(),'./source/power')
cmd1 = power_dir+'/get_pl.exe'
cmd2 = power_dir+'/get_cross.exe'
cldir = out_dir+'/cls'
try:
  os.makedirs(cldir)
except:
  pass
 
################################################################
### basically you do not have to edit followings
################################################################
## checking array size
if(check):
    if(len(zStart_arr) != NboxGalaxy):
        print('size of zStart_arr is not NboxGalaxy')
        sys.exit()

    if(len(zEnd_arr) != NboxGalaxy):
        print('size of zEnd_arr is not NboxGalaxy')
        sys.exit()

    if(len(ng_arr) != NboxGalaxy):
        print('size of ng_arr is not NboxGalaxy')
        sys.exit()

    if(len(bias_arr) != NboxGalaxy):
        print('size of bias_arr is not NboxGalaxy')
        sys.exit()

    if(len(nMassSheets) != NboxAll):
        print('size of nMassSheets is not NboxFore+NboxGalaxy+NboxBack')
        sys.exit()

### set parameters
om0 = (oc0h2+ob0h2+mnu/93.1)/h0**2
cosmo = FlatLambdaCDM(H0=h0*100., Om0=om0)
HOME = os.getcwd()

random.seed(seed)

Lx = np.zeros(NboxAll, dtype=np.float)
Ly = np.zeros(NboxAll, dtype=np.float)
Lz = np.zeros(NboxAll, dtype=np.float)

# global setting for 'lognormal_galaxies'
global_lognormal_galaxies = 'gen_inputs = True\nrun_lognormal = True\ncalc_pk = False'

z_arr = np.zeros(NboxAll)
dzBack = (zEndBack-zEnd_arr[NboxGalaxy-1])/float(NboxBack)
source_z_weight = np.zeros(NboxAll) # weights for multiple source redshift case

'''
set box size, redshift and input matter power spectrum 
'''
for i in range(NboxAll):
    # redshift z and box length along the l.o.s
    if (i < NboxFore):
        Lz[i] = cosmo.comoving_distance(zStart_arr[0]).value*h0/float(NboxFore)
        z = z_at_value(cosmo.comoving_distance, (i+0.5)*Lz[i]*astropy.units.Mpc/h0)
        # box length perpendicular to the l.o.s
        Lx[i] = cosmo.comoving_distance(z).value*h0 \
               *np.tan(np.radians(OpeningAngle/2.0))*2.0
        Ly[i] = Lx[i]
        if i == 0:
            zmin = 0.0
        else:
            zmin = z_at_value(cosmo.comoving_distance, (i)*Lz[i]*astropy.units.Mpc/h0)
        zmax = z_at_value(cosmo.comoving_distance, (i+1.0)*Lz[i]*astropy.units.Mpc/h0)
        source_z_weight[i], tmp = quad(lambda x: pz(x), zmin, zmax)
    elif (NboxFore <= i and i < NboxFore+NboxGalaxy): 
        zStart = zStart_arr[i-NboxFore]
        zEnd = zEnd_arr[i-NboxFore]
        Lz[i] = cosmo.comoving_distance(zEnd).value*h0 \
               -cosmo.comoving_distance(zStart).value*h0
        chi = (cosmo.comoving_distance(zEnd).value*h0 \
              +cosmo.comoving_distance(zStart).value*h0)/2.
            # comoving distance to the box center
        z = z_at_value(cosmo.comoving_distance, chi*astropy.units.Mpc/h0)
        # box length perpendicular to the l.o.s
        Lx[i] = chi*np.tan(np.radians(OpeningAngle/2.0))*2.0
        Ly[i] = Lx[i]
        source_z_weight[i], tmp = quad(lambda x: pz(x), zStart, zEnd)
    elif (NboxFore+NboxGalaxy <= i):
        zStart = zEnd_arr[NboxGalaxy-1]+dzBack*(i-(NboxFore+NboxGalaxy))
        zEnd = zStart+dzBack
        Lz[i] = cosmo.comoving_distance(zEnd).value * h0 \
               -cosmo.comoving_distance(zStart).value * h0
        chi = (cosmo.comoving_distance(zEnd).value*h0 \
              +cosmo.comoving_distance(zStart).value*h0)/2.
            # comoving distance to the box center
        z = z_at_value(cosmo.comoving_distance, chi*astropy.units.Mpc/h0)
        Lx[i] = chi*np.tan(np.radians(OpeningAngle/2.0))*2.0
        Ly[i] = Lx[i]
        source_z_weight[i], tmp = quad(lambda x: pz(x), zStart, zEnd)
    z_arr[i] = z

    # generate input matter power spectrum
    mpk_fname = mpk_dir+'/'+prefix+'_'+str(i)+'_mpk.txt'
    mpk_fnames.append(mpk_fname)
    mpk = np.array([cosmo_class.pk(k*h0,z)*h0**3 for k in kh_arr])
    np.savetxt(mpk_fname,np.c_[kh_arr,mpk])

# save source z weights
aux_dir = out_dir+'/aux_files'
make_dir(aux_dir)
source_z_weight_ofname = aux_dir+'/zs_weights.txt'
np.savetxt(source_z_weight_ofname, source_z_weight)

# clear Class
cosmo_class.struct_cleanup()
 
###### generating init files for lognormal_galaxies
def gen_lg_inifiles(irlz):
    for i in range(NboxFore):
        z = z_arr[i]
        mpk_fname = mpk_dir+'/'+prefix+'_'+str(i)+'_mpk.txt'
        
        if(Lz[i]>Lx[i]):
            dMesh = Lx[i]/float(PnmaxFid)
            Pnmax = int(Lz[i]/dMesh)
        else:
            Pnmax = PnmaxFid
    
        if not check:
            output_str = 'out_dir = '+out_dir+'\n'\
                + 'z = '+str(z)+'\n'\
                + 'Lx = '+str(Lx[i])+'\n'\
                + 'Ly = '+str(Ly[i])+'\n'\
                + 'Lz = '+str(Lz[i])+'\n'\
                + 'Pnmax = '+str(Pnmax)+'\n'\
                + 'seed = '+str(int(10000000*random.random()))+'\n'\
                + 'inp_pk_fname = '+mpk_fname +'\n'\
                + 'output_gal = 0\n'\
                + 'output_matter = 1\n'\
                + 'oc0h2 = '+str(oc0h2)+'\n'\
                + 'mnu = '+str(mnu)+'\n'\
                + 'ns = '+str(ns)+'\n'\
                + 'lnAs = '+str(lnAs)+'\n'\
                + 'ob0h2 = '+str(ob0h2)+'\n'\
                + 'h0 = '+str(h0)+'\n'\
                + 'w = '+str(w)+'\n'\
                + 'run = '+str(run)+'\n'\
                + global_lognormal_galaxies
            
            f = open(lg_inifiles_name[i], "w")
            f.write(output_str)
            f.close()
    
    for i in range(NboxGalaxy):
        if(Lz[i+NboxFore]>Lx[i+NboxFore]):
            dMesh = Lx[i+NboxFore]/float(PnmaxFid)
            Pnmax = int(Lz[i+NboxFore]/dMesh)
        else:
            Pnmax = PnmaxFid
        Ngal = int(ng_arr[i] * Lx[i+NboxFore] * Ly[i+NboxFore] * Lz[i+NboxFore])
        z = z_arr[i+NboxFore]
        mpk_fname = mpk_dir+'/'+prefix+'_'+str(i+NboxFore)+'_mpk.txt'
    
        if not check:
            output_str = 'out_dir = '+out_dir+'\n'\
                + 'z = '+str(z)+'\n'\
                + 'Lx = '+str(Lx[i+NboxFore])+'\n'\
                + 'Ly = '+str(Ly[i+NboxFore])+'\n'\
                + 'Lz = '+str(Lz[i+NboxFore])+'\n'\
                + 'Pnmax = '+str(Pnmax)+'\n'\
                + 'seed = '+str(int(10000000*random.random()))+'\n'\
                + 'inp_pk_fname = '+mpk_fname +'\n'\
                + 'output_gal = 1\n'\
                + 'output_matter = 1\n'\
                + 'Ngalaxies = '+str(Ngal)+'\n'\
                + 'bias = '+str(bias_arr[i])+'\n'\
                + 'oc0h2 = '+str(oc0h2)+'\n'\
                + 'mnu = '+str(mnu)+'\n'\
                + 'ns = '+str(ns)+'\n'\
                + 'lnAs = '+str(lnAs)+'\n'\
                + 'ob0h2 = '+str(ob0h2)+'\n'\
                + 'h0 = '+str(h0)+'\n'\
                + 'w = '+str(w)+'\n'\
                + 'run = '+str(run)+'\n'\
                + global_lognormal_galaxies
            
            f = open(lg_inifiles_name[i+NboxFore], "w")
            f.write(output_str)
            f.close()
    
    for i in range(NboxBack):
        if(Lz[i+NboxFore+NboxGalaxy]>Lx[i+NboxFore+NboxGalaxy]):
            dMesh = Lx[i+NboxFore+NboxGalaxy]/float(PnmaxFid)
            Pnmax = int(Lz[i+NboxFore+NboxGalaxy]/dMesh)
        else:
            Pnmax = PnmaxFid
    
        # calc matter P(k)
        z = z_arr[i+NboxFore+NboxGalaxy]
        mpk_fname = mpk_dir+'/'+prefix+'_'+str(i+NboxFore+NboxGalaxy)+'_mpk.txt'
     
        if not check:
            output_str = 'out_dir = '+out_dir+'\n'\
                + 'z = '+str(z)+'\n'\
                + 'Lx = '+str(Lx[i+NboxFore+NboxGalaxy])+'\n'\
                + 'Ly = '+str(Ly[i+NboxFore+NboxGalaxy])+'\n'\
                + 'Lz = '+str(Lz[i+NboxFore+NboxGalaxy])+'\n'\
                + 'Pnmax = '+str(Pnmax)+'\n'\
                + 'seed = '+str(int(10000000*random.random()))+'\n'\
                + 'inp_pk_fname = '+mpk_fname+'\n'\
                + 'output_gal = 0\n'\
                + 'output_matter = 1\n'\
                + 'oc0h2 = '+str(oc0h2)+'\n'\
                + 'mnu = '+str(mnu)+'\n'\
                + 'ns = '+str(ns)+'\n'\
                + 'lnAs = '+str(lnAs)+'\n'\
                + 'ob0h2 = '+str(ob0h2)+'\n'\
                + 'h0 = '+str(h0)+'\n'\
                + 'w = '+str(w)+'\n'\
                + 'run = '+str(run)+'\n'\
                + global_lognormal_galaxies
            
            f = open(lg_inifiles_name[i+NboxFore+NboxGalaxy], "w")
            f.write(output_str)
            f.close()

###### visual check of raytracing geometry
if(check):
    import matplotlib.pyplot as plt
    lw = 0.8
    def drowbox(xs,depth,width,n, color="black"):
        plt.hlines([-width/2.0,width/2.0],xs,xs+depth, color, lw=lw)
        plt.vlines([xs,xs+depth],-width/2.0,width/2.0, color, lw=lw)
        dsheet = depth/float(n)
        divlines=np.arange(start=xs+dsheet, stop=xs+depth, step=dsheet)
        plt.vlines(divlines,-width/2.0,width/2.0,linestyles="dotted",lw=lw)

    def put_inp_pk_label(xs,ys,length,string):
        ax.annotate(string,xy=(xs,ys+length),xytext=(xs,ys),arrowprops=dict(arrowstyle='->'), rotation=-90, ha='center')

    fig = plt.figure(figsize=(8,2))
    ax = plt.subplot(111)
    ax.xaxis.tick_bottom()
    ax.minorticks_on()
    # ax.tick_params(which='minor',length=10)
    plt.xlim(0,cosmo.comoving_distance(zEndBack).value * h0 *1.02)
    plt.ylim(-max(Lx)/2.0*1.05, max(Lx)/2.0*1.05)
    ax.set_aspect('equal')
    
    xstart=0.0
    for i in range(NboxFore):
        drowbox(xstart,Lz[i],Lx[i], nMassSheets[i])
        # put_inp_pk_label(xstart+Lz[i]/2.0, -max(Lx), max(Lx)/2.0, inp_pk_arr[i])
        xstart+=Lz[i]

    for i in range(NboxGalaxy):
        xstart=cosmo.comoving_distance(zStart_arr[i]).value*h0
        drowbox(xstart, Lz[i+NboxFore], Lx[i+NboxFore], nMassSheets[i+NboxFore], "red")
        # put_inp_pk_label(xstart+Lz[i+NboxFore]/2.0, -max(Lx), max(Lx)/2.0, inp_pk_arr[i+NboxFore])

    for i in range(NboxBack):
        xstart=cosmo.comoving_distance(zEnd_arr[NboxGalaxy-1]+dzBack*i).value*h0
        drowbox(xstart, Lz[i+NboxFore+NboxGalaxy], Lx[i+NboxFore+NboxGalaxy], nMassSheets[i+NboxFore+NboxGalaxy])
        # put_inp_pk_label(xstart+Lz[i+NboxFore+NboxGalaxy]/2.0, -max(Lx), max(Lx)/2.0, inp_pk_arr[i+NboxFore+NboxGalaxy])

    # FoV lines
    plt.plot([0.0,cosmo.comoving_distance(z_arr[-1]).value * h0],
             [0.0,Lx[-1]/2.],'k:',lw=lw)
    plt.plot([0.0,cosmo.comoving_distance(z_arr[-1]).value * h0],
             [0.0,-Lx[-1]/2.],'k:',lw=lw)

    # put box index

    # label
    plt.xlabel('comoving distance [Mpc/h]')
    plt.ylabel(r'$L_x$, $L_y$ [Mpc/h]')
    plt.tight_layout()
    plt.show()    
    # plt.savefig('./light_cone.pdf')
    sys.exit()    

###### running lognormal_galaxies and generating mass sheets for Raytrix
psis_dir = out_dir+'/psis'
make_dir(psis_dir)
nSlice = sum(nMassSheets)

def run_lg_l2p(i):
    cmd = './run.py '+lg_inifiles_name[i]
    sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, cwd=lognormal_galaxies_dir)
    sp.wait()

    # run lognormal2psis
    input_lognormal = out_dir+'/lognormal/'+prefix+'_'+str(i)+'_density_lognormal_rlz0.bin'
    cmd = raytrix_dir + '/lognormal2psis_fftw.exe -s '+str(nMassSheets[i])+' -n '+str(PnmaxFid)
    sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
    sp.stdin.write((input_lognormal+'\n').encode())
    sp.stdin.flush()
    iStart = 0
    for j in range(i):
        iStart += nMassSheets[j]
    for j in range(nMassSheets[i]):
        mag = psis_dir + '/' +("%03d" % int(iStart+j))+'.mag'
        alp = psis_dir + '/' +("%03d" % int(iStart+j))+'.alp'
        flx = psis_dir + '/' +("%03d" % int(iStart+j))+'.flx'
        sp.stdin.write((mag+'\n').encode())
        sp.stdin.flush()
        sp.stdin.write((alp+'\n').encode())
        sp.stdin.flush()
        sp.stdin.write((flx+'\n').encode())
        sp.stdin.flush()
    sp.wait()

  
########## run calculation ################
n_rlz = ini['settings'].getint('n_rlz')
for irlz in range(n_rlz):
    # generate ini files
    gen_lg_inifiles(irlz)

    # run lognormal and convert to psis
    Parallel(n_jobs=num_para)(delayed(run_lg_l2p)(i) for i in range(NboxGalaxy+NboxFore+NboxBack))

    #### ray-tracing ####
    cmd = raytrix_dir + '/raytrix_kayo.exe'
    sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)

    tmp = "%s %d %d %d %f %f %f %f %f\n" % (prefix, PnmaxFid, nSlice, NgridRay, h0, om0, 1-om0, w, OpeningAngle*60.0/float(NgridRay))
    sp.stdin.write(tmp.encode())
    sp.stdin.flush()
    sp.stdin.write((psis_dir+'\n').encode())
    sp.stdin.flush()
    sp.stdin.write((lensdata_dir+'\n').encode())
    for i in range(NboxGalaxy+NboxFore+NboxBack):
        iStart = 0
        for j in range(i):
            iStart += nMassSheets[j]
        for j in range(nMassSheets[i]):
            tmp = "%d %03d 0 0 %f %f\n" % (iStart+j+1, iStart+j, Lx[i], Lz[i]/float(nMassSheets[i]))
            sp.stdin.write(tmp.encode())
            sp.stdin.flush()
    sp.wait()

    ###### make 2D gal map ######
    z_arr_gal = [0.5*(zStart_arr[i]+zEnd_arr[i]) for i in range(NboxGalaxy)]
    z_arr_gal_str = ['{:.2f}'.format(x) for x in z_arr_gal]
    for i in range(NboxGalaxy):
        galcat_in = out_dir+'/lognormal/'+prefix+'_'+str(i+NboxFore)+'_lognormal_rlz0.bin'
        galcat_out = galmap_dir+'/'+prefix+'_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'.dat'
        cmd = make_galmap_dir+'/gen_map_gal_2D.exe '
        cmd = cmd+galcat_in+' '+galcat_out+' '+str(PnmaxFid)
        print(cmd)
        sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
        sp.wait()

    # add maps weighting with source redshift distribution
    for i in range(NboxGalaxy):
        iFile_min = sum(nMassSheets[:NboxFore+i])
        nFile = nSlice-1
        ofname = lensdata_dir+'/'+prefix+'_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'.dat'
        cmd = raytrix_dir + '/sourcered.exe '
        cmd = cmd+lensdata_dir+'/'+prefix+" "\
                +str(iFile_min)+" "+str(source_z_weight_ofname)+" "+str(PnmaxFid)+" "+ofname
        print(cmd)
        sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
        sp.wait()

        # shape noise
        if ini['settings'].getboolean('add_shape_noise'):
            ifname = lensdata_dir+'/'+prefix+'_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'.dat'
            ofname = lensdata_dir+'/'+prefix+'_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'_sn.dat'
            ng = quad(lambda x: x*x*np.exp(-x/(1./3.)),zStart_arr[i],3.2)[0] \
                /quad(lambda x: x*x*np.exp(-x/(1./3.)),0.0,3.2)[0] \
                *ini['params'].getfloat('ng_source')
            seed_sn = i+irlz*NboxGalaxy
            cmd = raytrix_dir + '/addnoise.exe '+str(ng)+' '+ifname+' '+ofname+' '+str(seed_sn)+' '+str(PnmaxFid)+' '+str(OpeningAngle)
            # cmd = raytrix_dir + '/addnoise.exe '+str(ng)+' '+ifname+' '+ofname+' '+str(seed_sn)
            print(cmd)
            sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
            sp.wait()

    ###### get auto and cross cls ######
    # multiple source redshift
    for i in range(NboxGalaxy):
        # kk
        kmap_fname = lensdata_dir+'/'+prefix+'_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'.dat'
        kk_ofname = cldir+'/cl_kk_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'.txt'
        cmd_kk = cmd1+' '+kmap_fname+' '+kk_ofname+' '+str(PnmaxFid)+' '+str(OpeningAngle)
        print(cmd_kk)
        sp = subprocess.Popen(cmd_kk, shell=True, stdin=subprocess.PIPE)
        sp.wait()

        # gk cross
        gmap_fname = galmap_dir+'/'+prefix+'_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'.dat'
        gk_ofname = cldir+'/cl_gk_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'.txt'
        cmd_gk = cmd2+' '+kmap_fname+' '+gmap_fname+' '+gk_ofname+' '+str(PnmaxFid)+' '+str(OpeningAngle)
        print(cmd_gk)
        sp = subprocess.Popen(cmd_gk, shell=True, stdin=subprocess.PIPE)
        sp.wait()

        # kk, with shape noise
        if ini['settings'].getboolean('add_shape_noise'):
            kmap_fname = lensdata_dir+'/'+prefix+'_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'_sn.dat'
            kk_ofname = cldir+'/cl_kk_z'+z_arr_gal_str[i]+'_sn_rlz'+str(irlz)+'.txt'
            cmd_kk = cmd1+' '+kmap_fname+' '+kk_ofname+' '+str(PnmaxFid)+' '+str(OpeningAngle)
            print(cmd_kk)
            sp = subprocess.Popen(cmd_kk, shell=True, stdin=subprocess.PIPE)
            sp.wait()

            # gk cross
            gmap_fname = galmap_dir+'/'+prefix+'_z'+z_arr_gal_str[i]+'_rlz'+str(irlz)+'.dat'
            gk_ofname = cldir+'/cl_gk_z'+z_arr_gal_str[i]+'_sn_rlz'+str(irlz)+'.txt'
            cmd_gk = cmd2+' '+kmap_fname+' '+gmap_fname+' '+gk_ofname+' '+str(PnmaxFid)+' '+str(OpeningAngle)
            print(cmd_gk)
            sp = subprocess.Popen(cmd_gk, shell=True, stdin=subprocess.PIPE)
            sp.wait()

    # remove intermediate files
    cmd = 'rm '+galmap_dir+'/*'
    sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
    sp.wait()
    cmd = 'rm '+lensdata_dir+'/*.dat'
    sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
    sp.wait()
    cmd = 'rm '+out_dir+'/lognormal/*'
    sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
    sp.wait()

'''
Calculate theoretical power spectra
'''
if ini['settings'].getboolean('calc_theory'):
    # make dir
    model_out_dir = out_dir+'/model'
    make_dir(model_out_dir)
    
    # read zs weights
    weights_fname = out_dir+'/aux_files/zs_weights.txt'
    weights = np.loadtxt(weights_fname)
    
    # generate temporary file for Cl calculation
    out = np.c_[mpk_fnames,z_arr,Lz,weights]   
    ofname = './tmp.txt' 
    np.savetxt(ofname,out,fmt='%s')
     
    # cl kk, multi
    cmd = './source/power_theory/cl_kk_multi.exe tmp.txt '
    imax = NboxAll
    for i in range(NboxGalaxy):
        imin = NboxFore+i
        ofname = model_out_dir+'/cl_kk_'+prefix+'_imin'+str(imin)+'.txt '
        cmd1 = cmd+ofname+str(PnmaxFid)+' '+str(OpeningAngle)+' '+str(om0)+' '+str(h0)+' '+str(imin)+' '+str(imax)
        print(cmd1)
        sp = subprocess.Popen(cmd1, shell=True, stdin=subprocess.PIPE)
        sp.wait()
    
    # calc 3D matter-galaxy spectra
    for i in range(NboxGalaxy):
        print('calculate 3D galaxy-matter spectra for '+str(i+NboxFore)+'-th box. Takes few minutes...')
        # calc matter-galaxy correlation function
        cmd = [lognormal_galaxies_dir+'/aux_codes/calc_xi_gm',
               out_dir+'/inputs/'+prefix+'_'+str(i+NboxFore)+'_pkG.dat',
               out_dir+'/inputs/'+prefix+'_'+str(i+NboxFore)+'_mpkG.dat',
               '2','1 2','50']
        cmd = ' '.join(cmd)
        subprocess.run(cmd,shell=True)
        
        # calc matter-galaxy power spectrum
        cmd = [lognormal_galaxies_dir+'/aux_codes/calc_pk',
               './xi_gm_kmax50.dat',
               '2','1 2','1.0','1500']
        cmd = ' '.join(cmd)
        subprocess.run(cmd,shell=True)
    
        # save cross P(k)
        cpk_fname = out_dir+'/inputs/'+prefix+'_'+str(i+NboxFore)+'_cpk.txt'
        cmd = 'mv pk_rmax1500_b1.dat '+cpk_fname
        subprocess.run(cmd,shell=True)
        mpk_fnames[i+NboxFore] = cpk_fname
    
    # cl gk, multi
    out = np.c_[mpk_fnames,z_arr,Lz,weights]   
    ofname = './tmp.txt' 
    np.savetxt(ofname,out,fmt='%s')
    cmd = './source/power_theory/cl_gk_multi.exe tmp.txt '
    imax = NboxAll
    for i in range(NboxGalaxy):
        imin = NboxFore+i
        ofname = model_out_dir+'/cl_gk_'+prefix+'_i'+str(imin)+'.txt '
        izg = NboxFore+i
        bias = 1.0
        cmd1 = cmd+ofname+str(PnmaxFid)+' '+str(OpeningAngle)+' '+str(om0)+' '+str(h0)+' '+str(imin)+' '+str(imax)+' '+str(izg)+' '+str(bias)
        print(cmd1)
        sp = subprocess.Popen(cmd1, shell=True, stdin=subprocess.PIPE)
        sp.wait()
    
    # remove temporary file
    cmd = 'rm ./tmp.txt'
    sp = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
